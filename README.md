*KANINO API*

OBSERVAÇÕES

ESTRUTURA

* Data - Acesso ao banco de dados.
* Entity - Entidades que representam a estrutura das tabelas do banco de dados.
* Repository - Métodos de manipulação dos dados.
* Domain - Camada inteligente do projeto, responsável pela regra de negócio.
* Exception - Exceções personalizas.
* Rest - Serviço REST.


====================================================================================================================


PROBLEMAS E SOLUÇÕES

EXCEPTION

Exceptionmapper não é chamada:

* Problema com classes providers - verificar se o package está incluso no web.xml <param-name>com.sun.jersey.config.property.packages</param-name>, observar que no log do Tomcat deve aparecer "Provider classes found:" com o seu package listado.