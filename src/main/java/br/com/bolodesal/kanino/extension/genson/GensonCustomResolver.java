package br.com.bolodesal.kanino.extension.genson;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

import com.owlike.genson.Genson;
import com.owlike.genson.GensonBuilder;

@Provider
public class GensonCustomResolver implements ContextResolver<Genson> {
  private final Genson genson = new GensonBuilder().setNullConverter(new NullConverter()).create();

  @Override
  public Genson getContext(Class<?> type) {
      return genson;
  }
}
