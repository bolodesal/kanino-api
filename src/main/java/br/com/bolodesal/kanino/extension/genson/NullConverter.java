package br.com.bolodesal.kanino.extension.genson;

import com.owlike.genson.Context;
import com.owlike.genson.Converter;
import com.owlike.genson.stream.ObjectReader;
import com.owlike.genson.stream.ObjectWriter;

public class NullConverter extends Object implements Converter<Object> {

	@Override
	public void serialize(Object object, ObjectWriter writer, Context ctx) throws Exception {
		writer.writeString("");
	}

	@Override
	public Object deserialize(ObjectReader reader, Context ctx) throws Exception {		
		return "";
	}

}
