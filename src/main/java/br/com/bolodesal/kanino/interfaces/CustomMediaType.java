package br.com.bolodesal.kanino.interfaces;

import javax.ws.rs.core.MediaType;

public interface CustomMediaType {
	String APPLICATION_JSON = MediaType.APPLICATION_JSON + ";charset=utf-8";
}
