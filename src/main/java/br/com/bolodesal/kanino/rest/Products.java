package br.com.bolodesal.kanino.rest;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import br.com.bolodesal.kanino.domain.ProductDomain;
import br.com.bolodesal.kanino.interfaces.CustomMediaType;

@Path("/products")
public class Products {
	
	@Context ServletContext context;
	
	ProductDomain productDomain = new ProductDomain();
    
	@GET
	@Path("/{id}")
	@Produces(CustomMediaType.APPLICATION_JSON)
	public Response getById(@PathParam("id") int id, @QueryParam("imageWidth") int imageWidth, @QueryParam("imageHeight") int imageHeight) {
		if(imageWidth == 0) imageWidth = 300;
		if(imageHeight == 0) imageHeight = 300;
		productDomain.setContext(context);
		return Response.status(200).entity(productDomain.getById(id, imageWidth, imageHeight)).build();	
	}
	
	@GET
	@Path("/pagination/{pageNumber}/{itensPerPage}")
	@Produces(CustomMediaType.APPLICATION_JSON)
	public Response getProductsPaginated(@PathParam("pageNumber") int pageNumber, @PathParam("itensPerPage") int itensPerPage, @QueryParam("imageWidth") int imageWidth, @QueryParam("imageHeight") int imageHeight){
		if(imageWidth == 0) imageWidth = 300;
		if(imageHeight == 0) imageHeight = 300;
		productDomain.setContext(context);
		return Response.status(200).entity(productDomain.getProductsPaginated(pageNumber, itensPerPage, imageWidth, imageHeight)).build();	
	}
	
	@GET
	@Path("/categoryByOrderId/{categoryId}/{qtdItens}/{lastProductOrderId}")
	@Produces(CustomMediaType.APPLICATION_JSON)
	public Response getProductsByCategoryId(@PathParam("categoryId") int categoryId, @PathParam("qtdItens") int qtdItens, @PathParam("lastProductOrderId") int lastProductOrderId, @QueryParam("imageWidth") int imageWidth, @QueryParam("imageHeight") int imageHeight){
		if(imageWidth == 0) imageWidth = 300;
		if(imageHeight == 0) imageHeight = 300;
		productDomain.setContext(context);
		return Response.status(200).entity(productDomain.getByCategoryId(categoryId, qtdItens, lastProductOrderId, imageWidth, imageHeight)).build();	
	}
	
	@GET
	@Path("/category/{categoryId}/{pageNumber}/{itensPerPage}")
	@Produces(CustomMediaType.APPLICATION_JSON)
	public Response getProductsPaginatedByCategoryId(@PathParam("categoryId") int categoryId, @PathParam("pageNumber") int pageNumber, @PathParam("itensPerPage") int itensPerPage, @QueryParam("imageWidth") int imageWidth, @QueryParam("imageHeight") int imageHeight){
		if(imageWidth == 0) imageWidth = 300;
		if(imageHeight == 0) imageHeight = 300;
		productDomain.setContext(context);
		return Response.status(200).entity(productDomain.getProductsPaginatedByCategoryId(categoryId, pageNumber, itensPerPage, imageWidth, imageHeight)).build();	
	}
}