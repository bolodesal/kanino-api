package br.com.bolodesal.kanino.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import br.com.bolodesal.kanino.data.model.Auth;
import br.com.bolodesal.kanino.data.model.GlobalResponse;
import br.com.bolodesal.kanino.domain.CustomerDomain;
import br.com.bolodesal.kanino.interfaces.CustomMediaType;

@Path("/login")
public class Login {
	
	CustomerDomain customerDomain = new CustomerDomain();
		
	@POST
	@Consumes(CustomMediaType.APPLICATION_JSON)
	public Response login(Auth auth){
		GlobalResponse<?> response = customerDomain.authCustomer(auth);
		return Response.status(response.getStatus()).entity(response).build();
	}
	
	
}
