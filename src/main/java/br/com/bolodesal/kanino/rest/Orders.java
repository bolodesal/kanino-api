package br.com.bolodesal.kanino.rest;

import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.bolodesal.kanino.data.entity.Order;
import br.com.bolodesal.kanino.data.model.GlobalResponse;
import br.com.bolodesal.kanino.data.model.OrderArray;
import br.com.bolodesal.kanino.domain.OrderDomain;
import br.com.bolodesal.kanino.exception.DataNotFoundException;

@Path("/orders")
public class Orders {

	@Context ServletContext context;
	
	OrderDomain orderDomain = new OrderDomain();
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOrder(@PathParam("id") int id) throws DataNotFoundException{
		return Response.status(200).entity(orderDomain.getOrder(id)).build();
	}
	
	@GET
	@Path("/details/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOrderDetails(@PathParam("id") int id, @QueryParam("imageWidth") int imageWidth, @QueryParam("imageHeight") int imageHeight) throws DataNotFoundException{
		if(imageWidth == 0) imageWidth = 300;
		if(imageHeight == 0) imageHeight = 300;
		orderDomain.setContext(context);
		return Response.status(200).entity(orderDomain.getOrderDetails(id, imageWidth, imageHeight)).build();
	}
	
	@GET
	@Path("/customer/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOrdersByCustomer(@PathParam("id") int id) throws DataNotFoundException{
		return Response.status(200).entity(orderDomain.getOrdersByCustomer(id)).build();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveOrder(OrderArray order) throws SQLException{
		GlobalResponse<?> response = orderDomain.saveOrder(order);
		return Response.status(response.getStatus()).entity(response).build();
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateOrder(Order order){
		GlobalResponse<?> response = orderDomain.updateOrder(order);
		return Response.status(response.getStatus()).entity(response).build();
	}
}