package br.com.bolodesal.kanino.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import br.com.bolodesal.kanino.domain.CategoryDomain;
import br.com.bolodesal.kanino.interfaces.CustomMediaType;

@Path("/categories")
public class Categories {

	CategoryDomain categoriesDomain = new CategoryDomain();
	
	@GET
	@Path("/")
	@Produces(CustomMediaType.APPLICATION_JSON)
	public Response getCategories(){
		return Response.status(200).entity(categoriesDomain.getCategories()).build();
	}
}
