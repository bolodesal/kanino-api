package br.com.bolodesal.kanino.rest;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import br.com.bolodesal.kanino.domain.SearchDomain;
import br.com.bolodesal.kanino.interfaces.CustomMediaType;

@Path("/search")
public class Search {
	
	@Context ServletContext context;
	
	SearchDomain searchDomain = new SearchDomain();
	
	@GET
	@Path("/pagination/{pageNumber}/{itensPerPage}")
	@Produces(CustomMediaType.APPLICATION_JSON)
	public Response getProducts(@QueryParam("search") String search, @PathParam("pageNumber") int pageNumber, @PathParam("itensPerPage") int itensPerPage, @QueryParam("imageWidth") int imageWidth, @QueryParam("imageHeight") int imageHeight) {
		if(imageWidth == 0) imageWidth = 300;
		if(imageHeight == 0) imageHeight = 300;
		searchDomain.setContext(context);
		return Response.status(200).entity(searchDomain.getProducts(search, pageNumber, itensPerPage, imageWidth, imageHeight)).build();	
	}
	
	@GET
	@Path("/productsName")
	@Produces(CustomMediaType.APPLICATION_JSON)
	public Response getProductsName() {
		return Response.status(200).entity(searchDomain.getAllProductsName()).build();	
	}
}
