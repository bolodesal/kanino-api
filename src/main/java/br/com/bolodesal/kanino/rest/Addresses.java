package br.com.bolodesal.kanino.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.bolodesal.kanino.data.entity.Address;
import br.com.bolodesal.kanino.data.model.GlobalResponse;
import br.com.bolodesal.kanino.domain.AddressDomain;
import br.com.bolodesal.kanino.exception.DataNotFoundException;

@Path("/addresses")
public class Addresses {

	AddressDomain addressDomain = new AddressDomain();
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAddresses(@PathParam("id") int id) throws DataNotFoundException{
		return Response.status(200).entity(addressDomain.getAddress(id)).build();
	}
	
	@GET
	@Path("/customer/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAddressesByCostumer(@PathParam("id") int id) throws DataNotFoundException{
		return Response.status(200).entity(addressDomain.getAddressesByCostumerId(id)).build();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveAddress(Address address){
		GlobalResponse<?> response = addressDomain.saveAddress(address);
		return Response.status(response.getStatus()).entity(response).build();
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateAddress(Address address){
		GlobalResponse<?> response = addressDomain.updateAddress(address);
		return Response.status(response.getStatus()).entity(response).build();
	}
	
	@DELETE
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteAddress(@PathParam("id") int id){
		GlobalResponse<?> response = addressDomain.deleteAddress(id);
		return Response.status(response.getStatus()).entity(response).build();
	}
}
