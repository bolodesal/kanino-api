package br.com.bolodesal.kanino.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.bolodesal.kanino.data.entity.Customer;
import br.com.bolodesal.kanino.data.model.CustomerAddress;
import br.com.bolodesal.kanino.data.model.GlobalResponse;
import br.com.bolodesal.kanino.domain.CustomerDomain;

@Path("/customers")
public class Customers {
	
	CustomerDomain customerDomain = new CustomerDomain();

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCustomer(@PathParam("id") int id) {
		return Response.status(200).entity(customerDomain.getCustomer(id)).build();
	}
	
	@GET
	@Path("/hasByEmail/{email}/{idUser}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response hasCustomerByEmail(@PathParam("email") String emailCliente, @PathParam("idUser") int idUser) {
		if(emailCliente == null) emailCliente = "";
		GlobalResponse<?> response = customerDomain.hasCustomerByEmail(emailCliente, idUser);
		return Response.status(response.getStatus()).entity(response).build();
	}
	
	@GET
	@Path("/hasByCPF/{CPF}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response hasCustomerByCPF(@PathParam("CPF") String CPF) {
		if(CPF == null) CPF = "";
		GlobalResponse<?> response = customerDomain.hasCustomerByCPF(CPF);
		return Response.status(response.getStatus()).entity(response).build();
	}
	
	@POST
	@Path("/address")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveCustomerAddress(CustomerAddress customerAddress) {
		GlobalResponse<?> response = customerDomain.saveCustomerAddress(customerAddress);
		return Response.status(response.getStatus()).entity(response).build();
		
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveCustomer(Customer customer) {
		GlobalResponse<?> response = customerDomain.saveCustomer(customer);
		return Response.status(response.getStatus()).entity(response).build();
		
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateCustomer(Customer customer) {
		GlobalResponse<?> response = customerDomain.updateCustomer(customer);
		return Response.status(response.getStatus()).entity(response).build();		
	}
}