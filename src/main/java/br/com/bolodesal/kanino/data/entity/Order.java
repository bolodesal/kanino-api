package br.com.bolodesal.kanino.data.entity;

import java.util.Date;

import com.owlike.genson.annotation.JsonDateFormat;
import com.owlike.genson.annotation.JsonIgnore;


public class Order {
	private int idPedido;
	private int idCliente;
	private int idStatus;
	private Date dataPedido;
	private int idTipoPagto;
	private int idEndereco;
	private int idAplicacao;
	private String enderecoCompleto;
	
	//Adicionados 26/11
	private int totalItens;
	private double valorTotal;

	
	//Atributos para retorno amig�vel
	private String descTipoPagto;
	private String statusPedido;

	
	public int getIdPedido() {
		return idPedido;
	}
	
	public void setIdPedido(int idPedido) {
		this.idPedido = idPedido;
	}

	public int getIdCliente() {
		return idCliente;
	}
	
	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}

	@JsonDateFormat(value = "ddMMyyyy HH:mm:ss")
	public Date getDataPedido() {
		return dataPedido;
	}

	@JsonDateFormat(value = "ddMMyyyy HH:mm:ss")
	public void setDataPedido(Date dataPedido) {
		this.dataPedido = dataPedido;
	}	

	public int getIdEndereco() {
		return idEndereco;
	}
	
	public void setIdEndereco(int idEndereco) {
		this.idEndereco = idEndereco;
	}
	
	public int getIdAplicacao() {
		return idAplicacao;
	}
	
	public void setIdAplicacao(int idAplicacao) {
		this.idAplicacao = idAplicacao;
	}
	
	public String getStatusPedido() {
		return statusPedido;
	}
	
	public void setStatusPedido(String statusPedido) {
		this.statusPedido = statusPedido;
	}
	
	public String getDescTipoPagto() {
		return descTipoPagto;
	}
	
	public void setDescTipoPagto(String descTipoPagto) {
		this.descTipoPagto = descTipoPagto;
	}

	
	public int getTotalItens() {
		return totalItens;
	}

	public void setTotalItens(int totalItens) {
		this.totalItens = totalItens;
	}

	public double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(double valorTotal) {
		this.valorTotal = valorTotal;
	}
	
	// Ignorados no retorno do objeto
	
	@JsonIgnore
	public int getIdTipoPagto() {
		return idTipoPagto;
	}
	
	public void setIdTipoPagto(int idTipoPagto) {
		this.idTipoPagto = idTipoPagto;
	}
	
	
	@JsonIgnore
	public int getIdStatus() {
		return idStatus;
	}
	
	public void setIdStatus(int idStatus) {
		this.idStatus = idStatus;
	}
	
	public String getEnderecoCompleto() {
		return enderecoCompleto;
	}
	
	public void setEnderecoCompleto(String enderecoCompleto) {
		this.enderecoCompleto = enderecoCompleto;
	}

	// HELPERS
	@JsonIgnore
	public java.sql.Timestamp getDataPedidoAsSqlTimestamp(){
		return new java.sql.Timestamp(dataPedido.getTime());
	}


}
