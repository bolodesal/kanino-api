package br.com.bolodesal.kanino.data.entity;

public class Product {	
	private int idProduto;
	private String nomeProduto;
	private String descProduto;
	private double precProduto;
	private double descontoPromocao;
	private int idCategoria;
	private char ativoProduto;
	private int idUsuario;
	private int qtdMinEstoque;
	private byte[] imagem;	
	
	public int getIdProduto() {
		return idProduto;
	}
	public void setIdProduto(int idProduto) {
		this.idProduto = idProduto;
	}
	public String getNomeProduto() {
		return nomeProduto;
	}
	public void setNomeProduto(String nomeProduto) {
		this.nomeProduto = nomeProduto;
	}
	public String getDescProduto() {
		return descProduto;
	}
	public void setDescProduto(String descProduto) {
		this.descProduto = descProduto;
	}
	public double getPrecProduto() {
		return precProduto;
	}
	public void setPrecProduto(double precProduto) {
		this.precProduto = precProduto;
	}
	public double getDescontoPromocao() {
		return descontoPromocao;
	}
	public void setDescontoPromocao(double descontoPromocao) {
		this.descontoPromocao = descontoPromocao;
	}
	public int getIdCategoria() {
		return idCategoria;
	}
	public void setIdCategoria(int idCategoria) {
		this.idCategoria = idCategoria;
	}
	public char getAtivoProduto() {
		return ativoProduto;
	}
	public void setAtivoProduto(char ativoProduto) {
		this.ativoProduto = ativoProduto;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public int getQtdMinEstoque() {
		return qtdMinEstoque;
	}
	public void setQtdMinEstoque(int qtdMinEstoque) {
		this.qtdMinEstoque = qtdMinEstoque;
	}
	public byte[] getImagem() {		
		return imagem;
	}
	public void setImagem(byte[] imagem) {
		this.imagem = imagem;
	}
	
	//HELPER	
	private int idOrdem;

	public int getIdOrdem() {
		return idOrdem;
	}
	public void setIdOrdem(int idOrdem) {
		this.idOrdem = idOrdem;
	}
	
	private String nomeCategoria;
	
	public String getNomeCategoria(){
		return nomeCategoria;
	}
	
	public void setNomeCategoria(String nomeCategoria){
		this.nomeCategoria = nomeCategoria;
	}
}
