package br.com.bolodesal.kanino.data.entity;

public class Address {
	private int idEndereco;
	private int idCliente;
	private String nomeEndereco;
	private String logradouroEndereco;
	private String numeroEndereco;
	private String cepEndereco;
	private String complementoEndereco;
	private String cidadeEndereco;
	private String paisEndereco;
	private String ufEndereco;
	public int getIdEndereco() {
		return idEndereco;
	}
	public void setIdEndereco(int idEndereco) {
		this.idEndereco = idEndereco;
	}
	public int getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}
	public String getNomeEndereco() {
		return nomeEndereco;
	}
	public void setNomeEndereco(String nomeEndereco) {
		this.nomeEndereco = nomeEndereco.trim();
	}
	public String getLogradouroEndereco() {
		return logradouroEndereco.trim();
	}
	public void setLogradouroEndereco(String logradouroEndereco) {
		this.logradouroEndereco = logradouroEndereco.trim();
	}
	public String getNumeroEndereco() {
		return numeroEndereco;
	}
	public void setNumeroEndereco(String numeroEndereco) {
		this.numeroEndereco = numeroEndereco.replaceAll("\\s+"," ");
	}
	public String getCepEndereco() {
		return cepEndereco;
	}
	public void setCepEndereco(String cepEndereco) {
		this.cepEndereco = cepEndereco.replaceAll("\\s+"," ");
	}
	public String getComplementoEndereco() {
		return complementoEndereco;
	}
	public void setComplementoEndereco(String complementoEndereco) {
		this.complementoEndereco = complementoEndereco.replaceAll("\\s+"," ");
	}
	public String getCidadeEndereco() {
		return cidadeEndereco;
	}
	public void setCidadeEndereco(String cidadeEndereco) {
		this.cidadeEndereco = cidadeEndereco.trim();
	}
	public String getPaisEndereco() {
		return paisEndereco;
	}
	public void setPaisEndereco(String paisEndereco) {
		this.paisEndereco = paisEndereco.trim();
	}
	public String getUfEndereco() {
		return ufEndereco;
	}
	public void setUfEndereco(String ufEndereco) {
		this.ufEndereco = ufEndereco.trim();
	}
	
	
}
