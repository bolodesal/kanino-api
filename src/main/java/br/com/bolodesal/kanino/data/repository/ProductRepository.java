package br.com.bolodesal.kanino.data.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


import br.com.bolodesal.kanino.data.entity.Product;
import br.com.bolodesal.kanino.data.model.ProductNameSearch;
import br.com.bolodesal.kanino.data.model.ProductsPaginated;

public class ProductRepository extends BaseRepository {
	
	public Product getById(int id){
		try {
			
			String sql = "SELECT * FROM Produto p INNER JOIN Categoria c ON p.idCategoria = c.idCategoria WHERE idProduto = ? AND ativoProduto = '1'";
			
			PreparedStatement ps = this.getDbKanino().prepareStatement(sql);
			ps.setMaxRows(1);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			
			if(rs.next())
			{
				Product product = new Product();
				product.setIdProduto(rs.getInt("idProduto"));
				product.setNomeProduto(rs.getString("nomeProduto"));
				product.setDescProduto(rs.getString("descProduto"));
				product.setPrecProduto(rs.getDouble("precProduto"));
				product.setDescontoPromocao(rs.getDouble("descontoPromocao"));
				product.setIdCategoria(rs.getInt("idCategoria"));
				product.setAtivoProduto(rs.getString("ativoProduto").charAt(0));
				product.setIdUsuario(rs.getInt("idUsuario"));
				product.setQtdMinEstoque(rs.getInt("qtdMinEstoque"));
				product.setImagem(rs.getBytes("imagem"));
				product.setNomeCategoria(rs.getString("nomeCategoria"));
				return product;
			}
		    
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			try{
				if(this.getDbKanino() != null){
					this.getDbKanino().close();
				}
			}catch(Exception e){
				
			}
		}
		
		return null;
	}

	public ProductsPaginated<Product> getProductsPaginatedByCategoryId(int categoryId, int pageNumber, int itensPerPage){
		try {
			
			String sql = "SELECT * FROM Produto p INNER JOIN Categoria c ON p.idCategoria = c.idCategoria WHERE p.idCategoria = ? AND ativoProduto = '1' ORDER BY idProduto OFFSET (? - 1) * ? ROWS FETCH NEXT ? ROWS ONLY";
						
			PreparedStatement ps = this.getDbKanino().prepareStatement(sql);
			ps.setInt(1, categoryId);
			ps.setInt(2, pageNumber);
			ps.setInt(3, itensPerPage);
			ps.setInt(4, itensPerPage);
			ResultSet rs = ps.executeQuery();
			
			ArrayList<Product> products = new ArrayList<Product>();
			
			while(rs.next())
			{
				Product product = new Product();
				product.setIdProduto(rs.getInt("idProduto"));
				product.setNomeProduto(rs.getString("nomeProduto"));
				product.setDescProduto(rs.getString("descProduto"));
				product.setPrecProduto(rs.getDouble("precProduto"));
				product.setDescontoPromocao(rs.getDouble("descontoPromocao"));
				product.setIdCategoria(rs.getInt("idCategoria"));
				product.setAtivoProduto(rs.getString("ativoProduto").charAt(0));
				product.setIdUsuario(rs.getInt("idUsuario"));
				product.setQtdMinEstoque(rs.getInt("qtdMinEstoque"));
				product.setImagem(rs.getBytes("imagem"));
				product.setNomeCategoria(rs.getString("nomeCategoria"));
				products.add(product);
			}
			
			sql = "SELECT Count(*) AS Total FROM Produto WHERE idCategoria = ? AND ativoProduto = '1'";
			
			ps = this.getDbKanino().prepareStatement(sql);
			ps.setInt(1, categoryId);
			rs = ps.executeQuery();
			
			int totalProducts = 0;
			
			if(rs.next())
			{
				totalProducts = rs.getInt("Total");
			}
			
			int lastPage = (totalProducts % itensPerPage) > 0 ? 1 : 0;
			
			ProductsPaginated<Product> productsPaginated = new ProductsPaginated<Product>();
			productsPaginated.totalProdutos = totalProducts;
			productsPaginated.ultimaPagina = (totalProducts / itensPerPage) + lastPage;
			productsPaginated.paginaAtual = pageNumber;
			productsPaginated.produtos = products;
			
			return productsPaginated;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			try{
				if(this.getDbKanino() != null){
					this.getDbKanino().close();
				}
			}catch(Exception e){
				
			}
		}
		
		return null;
	}
	
	public ArrayList<Product> getByCategoryId(int categoryId, int qtdItens, int lastProductOrderId){
		try {
			
			String sql = "SELECT %s * FROM (SELECT ROW_NUMBER() OVER (ORDER BY idProduto) AS idOrdem, * FROM Produto) AS Prod WHERE idCategoria = ? AND idOrdem > ? AND ativoProduto = '1'";
			
			String topStatement = qtdItens > 0 ? String.format("TOP %d", qtdItens) : "";
			sql = String.format(sql, topStatement);
			lastProductOrderId = lastProductOrderId > 0 ? lastProductOrderId : 0; 
			
			PreparedStatement ps = this.getDbKanino().prepareStatement(sql);
			ps.setInt(1, categoryId);
			ps.setInt(2, lastProductOrderId);
			ResultSet rs = ps.executeQuery();
			
			ArrayList<Product> products = new ArrayList<Product>();
			
			while(rs.next())
			{
				Product product = new Product();
				product.setIdProduto(rs.getInt("idProduto"));
				product.setNomeProduto(rs.getString("nomeProduto"));
				product.setDescProduto(rs.getString("descProduto"));
				product.setPrecProduto(rs.getDouble("precProduto"));
				product.setDescontoPromocao(rs.getDouble("descontoPromocao"));
				product.setIdCategoria(rs.getInt("idCategoria"));
				product.setAtivoProduto(rs.getString("ativoProduto").charAt(0));
				product.setIdUsuario(rs.getInt("idUsuario"));
				product.setQtdMinEstoque(rs.getInt("qtdMinEstoque"));
				product.setImagem(rs.getBytes("imagem"));
				product.setIdOrdem(rs.getInt("idOrdem"));
				products.add(product);
			}
			
			return products;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			try{
				if(this.getDbKanino() != null){
					this.getDbKanino().close();
				}
			}catch(Exception e){
				
			}
		}
		
		return null;
	}

	public ProductsPaginated<Product> getAllPaginated(int pageNumber, int itensPerPage){
		try {
			
			String sql = "SELECT * FROM Produto p INNER JOIN Categoria c ON p.idCategoria = c.idCategoria WHERE ativoProduto = '1' ORDER BY idProduto OFFSET (? - 1) * ? ROWS FETCH NEXT ? ROWS ONLY";
			
			PreparedStatement ps = this.getDbKanino().prepareStatement(sql);
			ps.setInt(1, pageNumber);
			ps.setInt(2, itensPerPage);
			ps.setInt(3, itensPerPage);
			ResultSet rs = ps.executeQuery();
			
			ArrayList<Product> products = new ArrayList<Product>();
			
			while(rs.next())
			{
				Product product = new Product();
				product.setIdProduto(rs.getInt("idProduto"));
				product.setNomeProduto(rs.getString("nomeProduto"));
				product.setDescProduto(rs.getString("descProduto"));
				product.setPrecProduto(rs.getDouble("precProduto"));
				product.setDescontoPromocao(rs.getDouble("descontoPromocao"));
				product.setIdCategoria(rs.getInt("idCategoria"));
				product.setAtivoProduto(rs.getString("ativoProduto").charAt(0));
				product.setIdUsuario(rs.getInt("idUsuario"));
				product.setQtdMinEstoque(rs.getInt("qtdMinEstoque"));
				product.setImagem(rs.getBytes("imagem"));
				products.add(product);
			}
			
			sql = "SELECT Count(*) AS Total FROM Produto WHERE ativoProduto = '1'";
			
			ps = this.getDbKanino().prepareStatement(sql);
			rs = ps.executeQuery();
			
			int totalProducts = 0;
			
			if(rs.next())
			{
				totalProducts = rs.getInt("Total");
			}
			
			int lastPage = (totalProducts % itensPerPage) > 0 ? 1 : 0;
			
			ProductsPaginated<Product> productsPaginated = new ProductsPaginated<Product>();
			productsPaginated.totalProdutos = totalProducts;
			productsPaginated.ultimaPagina = (totalProducts / itensPerPage) + lastPage;
			productsPaginated.paginaAtual = pageNumber;
			productsPaginated.produtos = products;
			
			return productsPaginated;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			try{
				if(this.getDbKanino() != null){
					this.getDbKanino().close();
				}
			}catch(Exception e){
				
			}
		}
		
		return null;
	}	

	public ProductsPaginated<Product> getAllPaginatedBySearch(String search, int pageNumber, int itensPerPage){
		try {
			
			String sql = "SELECT * FROM Produto p INNER JOIN Categoria c ON p.idCategoria = c.idCategoria WHERE ativoProduto = '1' AND nomeProduto LIKE ? OR descProduto LIKE ? ORDER BY idProduto OFFSET (? - 1) * ? ROWS FETCH NEXT ? ROWS ONLY";
			
			PreparedStatement ps = this.getDbKanino().prepareStatement(sql);
			ps.setString(1, "%" + search + "%");
			ps.setString(2, search);
			ps.setInt(3, pageNumber);
			ps.setInt(4, itensPerPage);
			ps.setInt(5, itensPerPage);
			ResultSet rs = ps.executeQuery();
			
			ArrayList<Product> products = new ArrayList<Product>();
			
			while(rs.next())
			{
				Product product = new Product();
				product.setIdProduto(rs.getInt("idProduto"));
				product.setNomeProduto(rs.getString("nomeProduto"));
				product.setDescProduto(rs.getString("descProduto"));
				product.setPrecProduto(rs.getDouble("precProduto"));
				product.setDescontoPromocao(rs.getDouble("descontoPromocao"));
				product.setIdCategoria(rs.getInt("idCategoria"));
				product.setAtivoProduto(rs.getString("ativoProduto").charAt(0));
				product.setIdUsuario(rs.getInt("idUsuario"));
				product.setQtdMinEstoque(rs.getInt("qtdMinEstoque"));
				product.setImagem(rs.getBytes("imagem"));
				products.add(product);
			}
			
			sql = "SELECT Count(*) AS Total FROM Produto WHERE ativoProduto = '1' AND nomeProduto LIKE ? OR descProduto LIKE ?";
			
			ps = this.getDbKanino().prepareStatement(sql);
			ps.setString(1, "%" + search + "%");
			ps.setString(2, search);
			rs = ps.executeQuery();
			
			int totalProducts = 0;
			
			if(rs.next())
			{
				totalProducts = rs.getInt("Total");
			}
			
			int lastPage = (totalProducts % itensPerPage) > 0 ? 1 : 0;
			
			ProductsPaginated<Product> productsPaginated = new ProductsPaginated<Product>();
			productsPaginated.totalProdutos = totalProducts;
			productsPaginated.ultimaPagina = (totalProducts / itensPerPage) + lastPage;
			productsPaginated.paginaAtual = pageNumber;
			productsPaginated.produtos = products;
			
			return productsPaginated;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			try{
				if(this.getDbKanino() != null){
					this.getDbKanino().close();
				}
			}catch(Exception e){
				
			}
		}
		
		return null;
	}
	
	public ArrayList<ProductNameSearch> getAllProductsName(){
		try {
			
			String sql = "SELECT nomeProduto FROM Produto WHERE ativoProduto = '1' ORDER BY idProduto";
			
			PreparedStatement ps = this.getDbKanino().prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			ArrayList<ProductNameSearch> products = new ArrayList<ProductNameSearch>();
			
			while(rs.next())
			{
				ProductNameSearch product = new ProductNameSearch();
				product.setNomeProduto(rs.getString("nomeProduto"));
				products.add(product);
			}
			
			return products;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			try{
				if(this.getDbKanino() != null){
					this.getDbKanino().close();
				}
			}catch(Exception e){
				
			}
		}
		
		return null;
	}
}