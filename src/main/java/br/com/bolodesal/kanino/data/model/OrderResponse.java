package br.com.bolodesal.kanino.data.model;

public class OrderResponse {
	public OrderSaved order;
	
	public String message;
	
	public OrderResponse(OrderSaved order, String message){
		this.order = order;
		this.message = message;
	}
}
