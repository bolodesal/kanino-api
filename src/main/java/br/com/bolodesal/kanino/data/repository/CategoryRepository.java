package br.com.bolodesal.kanino.data.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.bolodesal.kanino.data.entity.Category;

public class CategoryRepository extends BaseRepository {
	
	public ArrayList<Category> getAll(){
		try{
			
			String sql = "SELECT * FROM Categoria";
			
			PreparedStatement ps = this.getDbKanino().prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			ArrayList<Category> categories = new ArrayList<Category>();
			
			while(rs.next())
			{
				Category category = new Category();
				category.setIdCategoria(rs.getInt("idCategoria"));
				category.setNomeCategoria(rs.getString("nomeCategoria"));
				category.setDescCategoria(rs.getString("descCategoria"));
				categories.add(category);
			}
			
			return categories;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try{
				if(this.getDbKanino() != null){
					this.getDbKanino().close();
				}
			}catch(Exception e){
				
			}
		}
		
		return null;
	}
}
