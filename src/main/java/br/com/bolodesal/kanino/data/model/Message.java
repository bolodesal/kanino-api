package br.com.bolodesal.kanino.data.model;

public class Message {
	
	public String message;
	
	public Message(){}
	
	public Message(String message){
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}	
}
