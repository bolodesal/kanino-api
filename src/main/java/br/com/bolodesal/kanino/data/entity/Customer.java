package br.com.bolodesal.kanino.data.entity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.owlike.genson.annotation.JsonDateFormat;
import com.owlike.genson.annotation.JsonIgnore;

public class Customer {
	
	private int idCliente;
	
	private String nomeCompletoCliente;
	
	private String emailCliente;
	
	private String senhaCliente;
	
	private String cpfCliente;
	
	private String celularCliente;
	
	private String telComercialCliente;
	
	private String telResidencialCliente;
	
	private Date dtNascCliente;
	
	private boolean recebeNewsletter;
		
	public int getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}
	public String getNomeCompletoCliente() {
		return nomeCompletoCliente;
	}
	public void setNomeCompletoCliente(String nomeCompletoCliente) {
		this.nomeCompletoCliente = nomeCompletoCliente.trim();
	}
	public String getEmailCliente() {
		return emailCliente;
	}
	public void setEmailCliente(String emailCliente) {
		this.emailCliente = emailCliente.trim();
	}
	public String getSenhaCliente() {
		return senhaCliente;
	}
	public void setSenhaCliente(String senhaCliente) {
		this.senhaCliente = senhaCliente;
	}
	public String getCpfCliente() {
		return cpfCliente;
	}
	public void setCpfCliente(String cpfCliente) {
		this.cpfCliente = cpfCliente;
	}
	public String getCelularCliente() {
		return celularCliente;
	}
	public void setCelularCliente(String celularCliente) {
		this.celularCliente = celularCliente;
	}
	public String getTelComercialCliente() {
		return telComercialCliente;
	}
	public void setTelComercialCliente(String telComercialCliente) {
		this.telComercialCliente = telComercialCliente;
	}
	public String getTelResidencialCliente() {
		return telResidencialCliente;
	}
	public void setTelResidencialCliente(String telResidencialCliente) {
		this.telResidencialCliente = telResidencialCliente;
	}	
	@JsonDateFormat(value = "ddMMyyyy")
	public Date getDtNascCliente() {
		return dtNascCliente;
	}
	@JsonDateFormat(value = "ddMMyyyy")
	public void setDtNascCliente(Date dtNascCliente) {
		this.dtNascCliente = dtNascCliente;
	}
	public boolean isRecebeNewsletter() {
		return recebeNewsletter;
	}
	public void setRecebeNewsletter(boolean recebeNewsletter) {
		this.recebeNewsletter = recebeNewsletter;
	}
	
	//HELRPS
	
	@JsonIgnore
	public java.sql.Date getDtNascClienteAsSqlDate() {
	    SimpleDateFormat targetFormat = new SimpleDateFormat("yyyyMMdd" );
	    Date date = null;
	    try{
	    	date = targetFormat.parse(targetFormat.format(getDtNascCliente()));
	    } catch(ParseException e){
	    	date = dtNascCliente;
	    }
		return new java.sql.Date(date.getTime());
	}
}
