package br.com.bolodesal.kanino.data.model;

import br.com.bolodesal.kanino.data.entity.Customer;

public class AuthResponse {

	private boolean auth;

	private Customer customer;

	private String message;

	public boolean isAuth() {
		return auth;
	}

	public void setAuth(boolean auth) {
		this.auth = auth;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}	
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}	
}
