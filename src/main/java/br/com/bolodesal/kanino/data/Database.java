package br.com.bolodesal.kanino.data;

import java.sql.Connection;
import java.sql.DriverManager;

public class Database {
	
	private static Database conn = null;
	
	private Database () {};
	
	public static Database get () {
		if (conn == null)
			conn = new Database();
		return conn;
	}
			
	public Connection Connect(String DB_URL, String DB_USER, String DB_PASSWORD, String DB_NAME) throws Exception {
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		String connectionString = String.format("jdbc:sqlserver://%s;user=%s@%s;password=%s;database=%s", DB_URL, DB_USER, DB_URL, DB_PASSWORD, DB_NAME);
		Connection conn = DriverManager.getConnection(connectionString);
		return conn;
	}
}
