package br.com.bolodesal.kanino.data.model;

public class Auth {
	
	private String emailCliente;	
	
	private String senhaCliente;

	public String getEmailCliente() {
		return emailCliente;
	}
	
	public void setEmailCliente(String emailCliente) {
		this.emailCliente = emailCliente;
	}

	public String getSenhaCliente() {
		return senhaCliente;
	}

	public void setSenhaCliente(String senhaCliente) {
		this.senhaCliente = senhaCliente;
	}	
	
}
