package br.com.bolodesal.kanino.data.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import br.com.bolodesal.kanino.data.entity.Order;
import br.com.bolodesal.kanino.data.entity.OrderItem;

public class OrderRepository extends BaseRepository {

	public OrderRepository(){}
	
	public Order get(int idPedido){
		try {
			String sql = "SELECT * FROM Pedido p"
					+ " INNER JOIN Endereco e ON p.idEndereco = e.idEndereco"
					+ " INNER JOIN StatusPedido s ON p.idStatus = s.idStatus"
					+ " INNER JOIN TipoPagamento tp ON p.idTipoPagto = tp.idTipoPagto"
					+ " WHERE p.idPedido = ?";
			
			PreparedStatement ps = this.getDbKanino().prepareStatement(sql);
			ps.setMaxRows(1);
			ps.setInt(1, idPedido);
			ResultSet rs = ps.executeQuery();
			
			if(rs.next())
			{
				Order order = new Order();
				order.setIdPedido(rs.getInt("idPedido"));
				order.setIdCliente(rs.getInt("idCliente"));
				order.setStatusPedido(rs.getString("descStatus"));			
				order.setDataPedido(rs.getDate("dataPedido"));
				order.setDescTipoPagto(rs.getString("descTipoPagto"));
				order.setIdStatus(rs.getInt("idStatus"));
				order.setDataPedido(rs.getDate("dataPedido"));				
				order.setIdTipoPagto(rs.getInt("idTipoPagto"));
				order.setIdEndereco(rs.getInt("idEndereco"));
				order.setIdAplicacao(rs.getInt("idAplicacao"));
				String logradouroEndereco = rs.getString("logradouroEndereco");
				int numeroEndereco = rs.getInt("numeroEndereco");
				String cidadeEndereco = rs.getString("cidadeEndereco");
				String ufEndereco = rs.getString("UFEndereco");
				String paisEndereco = rs.getString("paisEndereco");
				String complementoEndereco = String.format(" %s", rs.getString("complementoEndereco") != null ? rs.getString("complementoEndereco") : "");
				String enderecoCompleto = String.format("%s, %d%s - %s/%s - %s", logradouroEndereco, numeroEndereco, complementoEndereco, cidadeEndereco, ufEndereco, paisEndereco);
				order.setEnderecoCompleto(enderecoCompleto);
				return order;
			}
		    
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try{
				if(this.getDbKanino() != null){
					this.getDbKanino().close();
				}
			}catch(Exception e){
				
			}
		}
		
		return null;
	}
	
	public ArrayList<Order> getOrdersByCustomer(int idCliente){
		ArrayList<Order> retorno = new ArrayList<Order>();
		
		try {
			String sql = "SELECT * FROM Pedido p"
					+ " INNER JOIN Endereco e ON p.idEndereco = e.idEndereco"
					+ " INNER JOIN StatusPedido s ON p.idStatus = s.idStatus"
					+ " INNER JOIN TipoPagamento tp ON p.idTipoPagto = tp.idTipoPagto"
					+ " WHERE p.idCliente = ?";
			
			PreparedStatement ps = this.getDbKanino().prepareStatement(sql);
			ps.setMaxRows(0);
			ps.setInt(1, idCliente);
			ResultSet rs = ps.executeQuery();
			
			
			
			while(rs.next())
			{
				Order order = new Order();
				
				order.setIdPedido(rs.getInt("idPedido"));
				order.setIdCliente(rs.getInt("idCliente"));
				order.setStatusPedido(rs.getString("descStatus"));			
				order.setDataPedido(rs.getDate("dataPedido"));
				order.setDescTipoPagto(rs.getString("descTipoPagto"));
				order.setIdEndereco(rs.getInt("idEndereco"));
				order.setIdAplicacao(rs.getInt("idAplicacao"));
				String logradouroEndereco = rs.getString("logradouroEndereco");
				int numeroEndereco = rs.getInt("numeroEndereco");
				String cidadeEndereco = rs.getString("cidadeEndereco");
				String ufEndereco = rs.getString("UFEndereco");
				String paisEndereco = rs.getString("paisEndereco");
				String complementoEndereco = String.format(" %s", rs.getString("complementoEndereco") != null ? rs.getString("complementoEndereco") : "");
				String enderecoCompleto = String.format("%s, %d%s - %s/%s - %s", logradouroEndereco, numeroEndereco, complementoEndereco, cidadeEndereco, ufEndereco, paisEndereco);
				order.setEnderecoCompleto(enderecoCompleto);
				
				order.setTotalItens(this.getTotalItens(order.getIdPedido()));
				order.setValorTotal(this.getValorTotal(order.getIdPedido()));
				
				retorno.add(order);
			}
			
			
		    
			return retorno;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			try{
				if(this.getDbKanino() != null){
					this.getDbKanino().close();
				}
			}catch(Exception e){
				
			}
		}
		
		return null;
	}

	public ArrayList<OrderItem> getDetails(int idPedido){
		
		ArrayList<OrderItem> retorno = new ArrayList<OrderItem>();
		
		try{
			String sql = "SELECT * FROM ItemPedido ip"
					+ " INNER JOIN Produto p"
					+ " ON ip.idProduto = p.idProduto"
					+ " WHERE ip.idPedido = ?";
			
			PreparedStatement ps = this.getDbKanino().prepareStatement(sql);
			ps.setMaxRows(0);
			ps.setInt(1, idPedido);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){
				OrderItem orderItem = new OrderItem();
				orderItem.setIdPedido(rs.getInt("idPedido"));
				orderItem.setIdProduto(rs.getInt("idProduto"));
				orderItem.setQtdProduto(rs.getInt("qtdProduto"));
				orderItem.setPrecoVendaItem(rs.getDouble("precoVendaItem"));
				orderItem.setNomeProduto(rs.getString("nomeProduto"));
				orderItem.setImagem(rs.getBytes("imagem"));
				retorno.add(orderItem);
			}
			
			return retorno;
			
		} catch (SQLException e){
			e.printStackTrace();
		} finally {
			try{
				if(this.getDbKanino() != null){
					this.getDbKanino().close();
				}
			}catch(Exception e){
				
			}
		}
		
		return null;
	}
	
	public double getValorTotal(int idPedido){
		
		double retorno = 0;
		
		try{
			String sql = "SELECT * FROM ItemPedido ip WHERE ip.idPedido = ?";
			
			PreparedStatement ps = this.getDbKanino().prepareStatement(sql);
			ps.setMaxRows(0);
			ps.setInt(1, idPedido);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){
				retorno += rs.getDouble("precoVendaItem") * rs.getInt("qtdProduto");
			}
			
			return retorno;
			
		} catch (SQLException e){
			e.printStackTrace();
		} finally {
			try{
				if(this.getDbKanino() != null){
					this.getDbKanino().close();
				}
			}catch(Exception e){
				
			}
		}
		
		return 0;
	}
	
	public int getTotalItens(int idPedido){
		
		int retorno = 0;
		
		try{
			String sql = "SELECT * FROM ItemPedido ip WHERE ip.idPedido = ?";
			
			PreparedStatement ps = this.getDbKanino().prepareStatement(sql);
			ps.setMaxRows(0);
			ps.setInt(1, idPedido);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){
				retorno += rs.getInt("qtdProduto");
			}
			
			return retorno;
			
		} catch (SQLException e){
			e.printStackTrace();
		} finally {
			try{
				if(this.getDbKanino() != null){
					this.getDbKanino().close();
				}
			}catch(Exception e){
				
			}
		}
		
		return 0;
	}
	
	public int save(Order order, ArrayList<OrderItem> orderItens) throws SQLException{
 
		int result = 0;
		Connection conn = null; // Declarando nova Connection devido a problemas com setar autocommit
		try {
			
			String sql = "INSERT INTO Pedido (idCliente, idStatus, dataPedido, idTipoPagto, idEndereco, idAplicacao) VALUES(?, ?, ?, ?, ?, ?)";
			
			
			conn = this.getDbKanino();
			conn.setAutoCommit(false);

			PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			PreparedStatement ps2;
			ps.setInt(1, order.getIdCliente());
			ps.setInt(2, order.getIdStatus());
			ps.setTimestamp(3, order.getDataPedidoAsSqlTimestamp());
			ps.setInt(4, order.getIdTipoPagto());
			ps.setInt(5, order.getIdEndereco());
			ps.setInt(6, order.getIdAplicacao());
			
			result = ps.executeUpdate();
			
			// Pega �ltima linha inserida
			ResultSet newRow = ps.getGeneratedKeys();
			// Pega �ltimo idPedido
			int newIdPedido = 0;
			if (newRow.next()){
				newIdPedido = newRow.getInt("GENERATED_KEYS");
			}
			
			String sql2 = "INSERT INTO ItemPedido (idProduto, idPedido, qtdProduto, precoVendaItem) VALUES (?, ?, ?, ?)";
			
			ps = conn.prepareStatement(sql2);
			
			// Seta id do pedido
			ps.setInt(2, newIdPedido);
			for(OrderItem orderItem : orderItens){
				ps.setInt(1, orderItem.getIdProduto());
				ps.setInt(3, orderItem.getQtdProduto());
				ps.setDouble(4, orderItem.getPrecoVendaItem());
				ps.executeUpdate();
				
				
				//Pega quantidade atual de estoque;
				int qtdProdutoAtual = this.verificaQtdEstoque(orderItem.getIdProduto());
				int diferenca = qtdProdutoAtual - orderItem.getQtdProduto();
				//Atualiza estoque
				String sql3 = "UPDATE estoque SET qtdProdutoDisponivel = ? WHERE idProduto = ?";
				
				ps2 = conn.prepareStatement(sql3);
				ps2.setInt(1, diferenca);
				ps2.setInt(2, orderItem.getIdProduto());
				ps2.executeUpdate();
			}
			
			
			
			result = newIdPedido;
			
			// Commit na transaacao se tudo deu certo
			conn.commit();
			
		} catch (SQLException e) {
			// Rollback na transacao se falhar algum ponto
			e.printStackTrace();
			conn.rollback();			
		} finally{
			try{
				if(conn != null){
					conn.close();
				}
			}catch(Exception e){
				
			}			
		}
		
		return result;
	}

	public int update(Order order){
		int result = 0;
		
		try{
			String sql = "UPDATE Pedido SET "
					+ "idCliente = ?,"
					+ "idStatus = ?,"
					+ "dataPedido = ?,"
					+ "idTipoPagto = ?,"
					+ "idEndereco = ?,"
					+ "idAplicacao = ?"
					+ " WHERE idPedido = ?";		
			
			PreparedStatement ps = this.getDbKanino().prepareStatement(sql);
			ps.setMaxRows(1);
			
			ps.setInt(1, order.getIdCliente());
			ps.setInt(2, order.getIdStatus());
			ps.setTimestamp(3, order.getDataPedidoAsSqlTimestamp());
			ps.setInt(4, order.getIdTipoPagto());
			ps.setInt(5, order.getIdEndereco());
			ps.setInt(6, order.getIdAplicacao());
			ps.setInt(7, order.getIdPedido());
			
			result = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (this.getDbKanino() != null){
				try {
					this.getDbKanino().close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return result;
	}

	public int verificaQtdEstoque(int idProduto){
		int retorno = 0;
		
		try{
			String sql = "SELECT * FROM estoque WHERE idProduto = ?";
			
			PreparedStatement ps = this.getDbKanino().prepareStatement(sql);
			ps.setMaxRows(0);
			ps.setInt(1, idProduto);
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()){
				retorno = rs.getInt("qtdProdutoDisponivel");
			}
			
			return retorno;
			
		} catch (SQLException e){
			e.printStackTrace();
		} finally {
			try{
				if(this.getDbKanino() != null){
					this.getDbKanino().close();
				}
			}catch(Exception e){
				
			}
		}
		
		return 0;
	}
}
