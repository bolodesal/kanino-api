package br.com.bolodesal.kanino.data.model;

import java.util.ArrayList;

public class ProductsPaginated<T>{
	
	public int totalProdutos;
	
	public int paginaAtual;
	
	public int ultimaPagina;
	
	public ArrayList<T> produtos;
	
}
