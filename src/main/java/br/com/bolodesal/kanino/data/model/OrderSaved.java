package br.com.bolodesal.kanino.data.model;

public class OrderSaved {
	private int idPedido;

	public int getIdPedido() {
		return idPedido;
	}

	public void setIdPedido(int idPedido) {
		this.idPedido = idPedido;
	}
}
