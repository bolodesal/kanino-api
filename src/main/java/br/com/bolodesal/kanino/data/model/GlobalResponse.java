package br.com.bolodesal.kanino.data.model;

import com.sun.jersey.api.client.ClientResponse.Status;

public class GlobalResponse<T> {

	private Status status;	
	private T response;
	
	public GlobalResponse(){}
		
	public GlobalResponse(Status status, T response){
		this.status = status;
		this.response = response;		
	}
	
	public int getStatus() {
		return status.getStatusCode();
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public T getResponse() {
		return response;
	}

	public void setErrors(T response) {
		this.response = response;
	}
	
}
