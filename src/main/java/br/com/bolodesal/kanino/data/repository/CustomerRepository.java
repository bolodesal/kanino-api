package br.com.bolodesal.kanino.data.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import br.com.bolodesal.kanino.data.entity.*;
import br.com.bolodesal.kanino.data.model.Auth;
import br.com.bolodesal.kanino.data.model.CustomerAddress;
import br.com.bolodesal.kanino.domain.util.Validation;

public class CustomerRepository extends BaseRepository {	
	
	public Customer getById(int id){
		
		try {
			String sql = "SELECT * FROM Cliente WHERE idCliente = ?";
			
			PreparedStatement ps = this.getDbKanino().prepareStatement(sql);
			ps.setMaxRows(1);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			
			if(rs.next())
			{
				Customer customer = new Customer();
				customer.setIdCliente(rs.getInt("idCliente"));
				customer.setNomeCompletoCliente(rs.getString("nomeCompletoCliente"));
				customer.setEmailCliente(rs.getString("emailCliente"));
				customer.setSenhaCliente(rs.getString("senhaCliente"));
				customer.setCpfCliente(rs.getString("CPFCliente"));
				customer.setCelularCliente(rs.getString("celularCliente"));
				customer.setTelComercialCliente(rs.getString("telComercialCliente"));
				customer.setTelResidencialCliente(rs.getString("telResidencialCliente"));
				customer.setDtNascCliente(rs.getDate("dtNascCliente"));
				customer.setRecebeNewsletter(rs.getBoolean("recebeNewsLetter"));
				return customer;
			}
		    
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try{
				if(this.getDbKanino() != null){
					this.getDbKanino().close();
				}
			}catch(Exception e){
				
			}
		}
		
		return null;
	}
	
	public Customer getByEmail(String emailCliente){
		
		try {
			String sql = "SELECT * FROM Cliente WHERE emailCliente = ?";
			
			PreparedStatement ps = this.getDbKanino().prepareStatement(sql);
			ps.setMaxRows(1);
			ps.setString(1, emailCliente);
			ResultSet rs = ps.executeQuery();
			
			if(rs.next())
			{
				Customer customer = new Customer();
				customer.setIdCliente(rs.getInt("idCliente"));
				customer.setNomeCompletoCliente(rs.getString("nomeCompletoCliente"));
				customer.setEmailCliente(rs.getString("emailCliente"));
				customer.setSenhaCliente(rs.getString("senhaCliente"));
				customer.setCpfCliente(rs.getString("CPFCliente"));
				customer.setCelularCliente(rs.getString("celularCliente"));
				customer.setTelComercialCliente(rs.getString("telComercialCliente"));
				customer.setTelResidencialCliente(rs.getString("telResidencialCliente"));
				customer.setDtNascCliente(rs.getDate("dtNascCliente"));
				customer.setRecebeNewsletter(rs.getBoolean("recebeNewsLetter"));
				return customer;
			}
		    
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try{
				if(this.getDbKanino() != null){
					this.getDbKanino().close();
				}
			}catch(Exception e){
				
			}
		}
		
		return null;
	}
	
	public Customer getByCPF(String CPFCliente){
		
		try {
			String sql = "SELECT * FROM Cliente WHERE CPFCliente = ?";
			
			PreparedStatement ps = this.getDbKanino().prepareStatement(sql);
			ps.setMaxRows(1);
			ps.setString(1, CPFCliente);
			ResultSet rs = ps.executeQuery();
			
			if(rs.next())
			{
				Customer customer = new Customer();
				customer.setIdCliente(rs.getInt("idCliente"));
				customer.setNomeCompletoCliente(rs.getString("nomeCompletoCliente"));
				customer.setEmailCliente(rs.getString("emailCliente"));
				customer.setSenhaCliente(rs.getString("senhaCliente"));
				customer.setCpfCliente(rs.getString("CPFCliente"));
				customer.setCelularCliente(rs.getString("celularCliente"));
				customer.setTelComercialCliente(rs.getString("telComercialCliente"));
				customer.setTelResidencialCliente(rs.getString("telResidencialCliente"));
				customer.setDtNascCliente(rs.getDate("dtNascCliente"));
				customer.setRecebeNewsletter(rs.getBoolean("recebeNewsLetter"));
				return customer;
			}
		    
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try{
				if(this.getDbKanino() != null){
					this.getDbKanino().close();
				}
			}catch(Exception e){
				
			}
		}
		
		return null;
	}
	
	public Customer auth(Auth login){
		
		
		try {
			String sql = "SELECT * FROM Cliente WHERE emailCliente = ? AND senhaCliente = ?";
			
			PreparedStatement ps = this.getDbKanino().prepareStatement(sql);
			ps.setMaxRows(1);
			ps.setString(1, login.getEmailCliente());
			ps.setString(2, login.getSenhaCliente());
			ResultSet rs = ps.executeQuery();
			
			if(rs.next())
			{
				Customer customer = new Customer();
				customer.setIdCliente(rs.getInt("idCliente"));
				customer.setNomeCompletoCliente(rs.getString("nomeCompletoCliente"));
				customer.setEmailCliente(rs.getString("emailCliente"));
				customer.setSenhaCliente(rs.getString("senhaCliente"));
				customer.setCpfCliente(rs.getString("CPFCliente"));
				customer.setCelularCliente(rs.getString("celularCliente"));
				customer.setTelComercialCliente(rs.getString("telComercialCliente"));
				customer.setTelResidencialCliente(rs.getString("telResidencialCliente"));
				customer.setDtNascCliente(rs.getDate("dtNascCliente"));
				customer.setRecebeNewsletter(rs.getBoolean("recebeNewsLetter"));			
				return customer;
			}
		    
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try{
				if(this.getDbKanino() != null){
					this.getDbKanino().close();
				}
			}catch(Exception e){
				
			}
		}
		
		return null;
	}
	
	public int saveCustomerAddress(CustomerAddress customerAddress){
		int result = 0;
		Connection conn = this.getDbKanino();
		
		try {			
			conn.setAutoCommit(false);
			
			String sql = "INSERT INTO CLIENTE (nomeCompletoCliente, emailCliente, senhaCliente, CPFCliente, celularCliente, telComercialCliente, telResidencialCliente, dtNascCliente, recebeNewsletter) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
			
			PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);					
			ps.setString(1, customerAddress.cliente.getNomeCompletoCliente());
			ps.setString(2, customerAddress.cliente.getEmailCliente());
			ps.setString(3, customerAddress.cliente.getSenhaCliente());
			ps.setString(4, customerAddress.cliente.getCpfCliente());
			ps.setString(5, customerAddress.cliente.getCelularCliente());
			ps.setString(6, customerAddress.cliente.getTelComercialCliente());
			ps.setString(7, customerAddress.cliente.getTelResidencialCliente());
			ps.setDate(8, customerAddress.cliente.getDtNascClienteAsSqlDate());
			ps.setBoolean(9, customerAddress.cliente.isRecebeNewsletter());			
			ps.executeUpdate();
			
			ResultSet generatedKeys = ps.getGeneratedKeys();
			
			int idCliente = -1;
			
            if (generatedKeys.next()) {
            	idCliente = generatedKeys.getInt(1);
            	customerAddress.endereco.setIdCliente(idCliente);
            }
            else {
                throw new SQLException("Ops! Houve um problema no cadastro do usu�rio.");
            }
			
			sql = "INSERT INTO Endereco VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";		
			
			ps = conn.prepareStatement(sql);
			ps.setMaxRows(1);
			
			ps.setInt(1, customerAddress.endereco.getIdCliente());
			ps.setString(2, customerAddress.endereco.getNomeEndereco());
			ps.setString(3, customerAddress.endereco.getLogradouroEndereco());
			ps.setString(4, customerAddress.endereco.getNumeroEndereco());
			ps.setString(5, customerAddress.endereco.getCepEndereco());
			ps.setString(6, customerAddress.endereco.getComplementoEndereco());
			ps.setString(7, customerAddress.endereco.getCidadeEndereco());
			ps.setString(8, customerAddress.endereco.getPaisEndereco());
			ps.setString(9, customerAddress.endereco.getUfEndereco());
			ps.executeUpdate();
			
			result = idCliente;
			
			conn.commit();
		    
		} catch (SQLException e) {
			result = 0;
			try {
				conn.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			try{
				if(this.getDbKanino() != null){
					this.getDbKanino().close();
				}
			}catch(Exception e){
				
			}			
		}
		
		return result;
	}
	
	public int save(Customer customer){
		int result = 0;
		
		try {
			
			String sql = "INSERT INTO CLIENTE (nomeCompletoCliente, emailCliente, senhaCliente, CPFCliente, celularCliente, telComercialCliente, telResidencialCliente, dtNascCliente, recebeNewsletter) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
			
			PreparedStatement ps = this.getDbKanino().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);					
			ps.setString(1, customer.getNomeCompletoCliente());
			ps.setString(2, customer.getEmailCliente());
			ps.setString(3, customer.getSenhaCliente());
			ps.setString(4, customer.getCpfCliente());
			ps.setString(5, customer.getCelularCliente());
			ps.setString(6, customer.getTelComercialCliente());
			ps.setString(7, customer.getTelResidencialCliente());
			ps.setDate(8, customer.getDtNascClienteAsSqlDate());
			ps.setBoolean(9, customer.isRecebeNewsletter());			
			ps.executeUpdate();
			
			ResultSet generatedKeys = ps.getGeneratedKeys();
			
            if (generatedKeys.next()) {
            	customer.setIdCliente(generatedKeys.getInt(1));
            }
            else {
                throw new SQLException("Ops! Houve um problema no cadastro do usu�rio.");
            }
            
            result = customer.getIdCliente();
		    
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			try{
				if(this.getDbKanino() != null){
					this.getDbKanino().close();
				}
			}catch(Exception e){
				
			}			
		}
		
		return result;
	}

	public int update(Customer customer){
		if(new Validation().notNullOrEmpty(customer.getSenhaCliente(), ""))
		{
			return this.updateWithPassword(customer);
		}
		
		return this.updateWithOutPassword(customer);
	}
	
	private int updateWithPassword(Customer customer){
		int result = 0;
		
		try{
			String sql = "UPDATE CLIENTE SET "
					+ "nomeCompletoCliente = ?,"
					+ "emailCliente = ?,"
					+ "senhaCliente = ?,"
					+ "celularCliente = ?,"
					+ "telComercialCliente = ?,"
					+ "telResidencialCliente = ?,"
					+ "dtNascCliente = ?,"
					+ "recebeNewsLetter = ? "
					+ "where idCliente = ?";		
			
			PreparedStatement ps = this.getDbKanino().prepareStatement(sql);
			ps.setMaxRows(1);
			
			ps.setString(1, customer.getNomeCompletoCliente());
			ps.setString(2, customer.getEmailCliente());
			ps.setString(3, customer.getSenhaCliente());
			ps.setString(4, customer.getCelularCliente());
			ps.setString(5, customer.getTelComercialCliente());
			ps.setString(6, customer.getTelResidencialCliente());
			ps.setDate(7, customer.getDtNascClienteAsSqlDate());
			ps.setBoolean(8, customer.isRecebeNewsletter());
			ps.setInt(9, customer.getIdCliente());
			
			result = ps.executeUpdate();
		} catch (SQLException e) {
			if(e.getErrorCode() == 50000)
			{
				result = -1;
			}
			e.printStackTrace();
		} finally {
			if (this.getDbKanino() != null){
				try {
					this.getDbKanino().close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return result;
	}
	
	private int updateWithOutPassword(Customer customer){
		int result = 0;
		
		try{
			String sql = "UPDATE CLIENTE SET "
					+ "nomeCompletoCliente = ?,"
					+ "emailCliente = ?,"
					+ "celularCliente = ?,"
					+ "telComercialCliente = ?,"
					+ "telResidencialCliente = ?,"
					+ "dtNascCliente = ?,"
					+ "recebeNewsLetter = ? "
					+ "where idCliente = ?";		
			
			PreparedStatement ps = this.getDbKanino().prepareStatement(sql);
			ps.setMaxRows(1);
			
			ps.setString(1, customer.getNomeCompletoCliente());
			ps.setString(2, customer.getEmailCliente());
			ps.setString(3, customer.getCelularCliente());
			ps.setString(4, customer.getTelComercialCliente());
			ps.setString(5, customer.getTelResidencialCliente());
			ps.setDate(6, customer.getDtNascClienteAsSqlDate());
			ps.setBoolean(7, customer.isRecebeNewsletter());
			ps.setInt(8, customer.getIdCliente());
			
			result = ps.executeUpdate();
		} catch (SQLException e) {
			if(e.getErrorCode() == 50000)
			{
				result = -1;
			}
			e.printStackTrace();
		} finally {
			if (this.getDbKanino() != null){
				try {
					this.getDbKanino().close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return result;
	}
}
