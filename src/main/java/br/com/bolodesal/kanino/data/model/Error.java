package br.com.bolodesal.kanino.data.model;

public class Error<T> {
		
	private T errors;

	public T getErrors() {
		return errors;
	}

	public void setErrors(T errors) {
		this.errors = errors;
	}
	
	public Error(){
		
	}
	
	public Error(T errors){
		this.errors = errors;
	}
	
}
