package br.com.bolodesal.kanino.data.model;

public class CustomerSaved {
	
	private int idCliente;

	public int getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}
	
}
