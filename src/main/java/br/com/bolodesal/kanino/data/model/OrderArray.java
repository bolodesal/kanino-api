package br.com.bolodesal.kanino.data.model;

import java.util.ArrayList;

import br.com.bolodesal.kanino.data.entity.Order;
import br.com.bolodesal.kanino.data.entity.OrderItem;

public class OrderArray {
	private Order order;
	private ArrayList<OrderItem> orderItens;
	
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	public ArrayList<OrderItem> getOrderItens() {
		return orderItens;
	}
	public void setOrderItens(ArrayList<OrderItem> orderItens) {
		this.orderItens = orderItens;
	}
}
