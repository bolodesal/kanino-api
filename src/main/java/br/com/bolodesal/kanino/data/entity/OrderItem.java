package br.com.bolodesal.kanino.data.entity;

public class OrderItem {
	private int idProduto;
	private String nomeProduto;
	private int idPedido;
	private int qtdProduto;
	private Double precoVendaItem;
	private byte[] imagem;
	
	public int getIdProduto() {
		return idProduto;
	}
	
	public void setIdProduto(int idProduto) {
		this.idProduto = idProduto;
	}
	
	public int getIdPedido() {
		return idPedido;
	}
	
	public void setIdPedido(int idPedido) {
		this.idPedido = idPedido;
	}
	
	public int getQtdProduto() {
		return qtdProduto;
	}
	
	public void setQtdProduto(int qtdProduto) {
		this.qtdProduto = qtdProduto;
	}
	
	public Double getPrecoVendaItem() {
		return precoVendaItem;
	}
	
	public void setPrecoVendaItem(Double precoVendaItem) {
		this.precoVendaItem = precoVendaItem;
	}

	public String getNomeProduto() {
		return nomeProduto;
	}

	public void setNomeProduto(String nomeProduto) {
		this.nomeProduto = nomeProduto;
	}

	public byte[] getImagem() {
		return imagem;
	}

	public void setImagem(byte[] imagem) {
		this.imagem = imagem;
	}
	
}
