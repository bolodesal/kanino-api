package br.com.bolodesal.kanino.data.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.bolodesal.kanino.data.entity.Address;

public class AddressRepository extends BaseRepository{
	
	public AddressRepository(){}
	
	public Address getAddress(int id){
				
		try {
			String sql = "SELECT * FROM Endereco WHERE idEndereco = ?";
			
			PreparedStatement ps = this.getDbKanino().prepareStatement(sql);
			ps.setMaxRows(0);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			
			Address address = new Address();
			
			if(rs.next())
			{				
				address.setIdEndereco(rs.getInt("IdEndereco"));
				address.setIdCliente(rs.getInt("IdCliente"));
				address.setNomeEndereco(rs.getString("NomeEndereco"));
				address.setLogradouroEndereco(rs.getString("LogradouroEndereco"));
				address.setNumeroEndereco(rs.getString("NumeroEndereco"));
				address.setCepEndereco(rs.getString("CEPEndereco"));
				address.setComplementoEndereco(rs.getString("ComplementoEndereco"));
				address.setCidadeEndereco(rs.getString("CidadeEndereco"));
				address.setPaisEndereco(rs.getString("PaisEndereco"));
				address.setUfEndereco(rs.getString("UfEndereco"));
			}
		    
			return address;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			try{
				if(this.getDbKanino() != null){
					this.getDbKanino().close();
				}
			}catch(Exception e){
				
			}
		}
		
		return null;
	}
	
	public ArrayList<Address> getAddressesByCostumerId(int idCliente){
		
		ArrayList<Address> retorno = new ArrayList<Address>();
		
		try {
			String sql = "SELECT * FROM Endereco WHERE idCliente = ?";
			
			PreparedStatement ps = this.getDbKanino().prepareStatement(sql);
			ps.setMaxRows(0);
			ps.setInt(1, idCliente);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next())
			{
				Address address = new Address();
				address.setIdEndereco(rs.getInt("IdEndereco"));
				address.setIdCliente(rs.getInt("IdCliente"));
				address.setNomeEndereco(rs.getString("NomeEndereco"));
				address.setLogradouroEndereco(rs.getString("LogradouroEndereco"));
				address.setNumeroEndereco(rs.getString("NumeroEndereco"));
				address.setCepEndereco(rs.getString("CEPEndereco"));
				address.setComplementoEndereco(rs.getString("ComplementoEndereco"));
				address.setCidadeEndereco(rs.getString("CidadeEndereco"));
				address.setPaisEndereco(rs.getString("PaisEndereco"));
				address.setUfEndereco(rs.getString("UfEndereco"));
				retorno.add(address);
			}
		    
			return retorno;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			try{
				if(this.getDbKanino() != null){
					this.getDbKanino().close();
				}
			}catch(Exception e){
				
			}
		}
		
		return null;
	}
	
	public int save(Address address){
		int result = 0;
		
		try{
			String sql = "INSERT INTO Endereco VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";		
			
			PreparedStatement ps = this.getDbKanino().prepareStatement(sql);
			ps.setMaxRows(1);
			
			ps.setInt(1, address.getIdCliente());
			ps.setString(2, address.getNomeEndereco());
			ps.setString(3, address.getLogradouroEndereco());
			ps.setString(4, address.getNumeroEndereco());
			ps.setString(5, address.getCepEndereco());
			ps.setString(6, address.getComplementoEndereco());
			ps.setString(7, address.getCidadeEndereco());
			ps.setString(8, address.getPaisEndereco());
			ps.setString(9, address.getUfEndereco());
			
			result = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (this.getDbKanino() != null){
				try {
					this.getDbKanino().close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return result;
	}
	
	public int update(Address address){
		int result = 0;
		
		try{
			String sql = "UPDATE Endereco SET "
					+ "nomeEndereco = ?,"
					+ "logradouroEndereco = ?,"
					+ "numeroEndereco = ?,"
					+ "CEPEndereco = ?,"
					+ "complementoEndereco = ?,"
					+ "cidadeEndereco = ?,"
					+ "paisEndereco = ?,"
					+ "UfEndereco = ? "
					+ "where idEndereco = ?";		
			
			PreparedStatement ps = this.getDbKanino().prepareStatement(sql);
			ps.setMaxRows(1);
			
			ps.setString(1, address.getNomeEndereco());
			ps.setString(2, address.getLogradouroEndereco());
			ps.setString(3, address.getNumeroEndereco());
			ps.setString(4, address.getCepEndereco());
			ps.setString(5, address.getComplementoEndereco());
			ps.setString(6, address.getCidadeEndereco());
			ps.setString(7, address.getPaisEndereco());
			ps.setString(8, address.getUfEndereco());
			ps.setInt(9, address.getIdEndereco());
			
			result = ps.executeUpdate();
		} catch (SQLException e) {
			if(e.getErrorCode() == 50000)
			{
				result = -1;
			}
			e.printStackTrace();
		} finally {
			if (this.getDbKanino() != null){
				try {
					this.getDbKanino().close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return result;
	}

	public int delete(int idEndereco){
		int result = 0;
		
		try{
			String sql = "DELETE FROM Endereco WHERE idEndereco = ?";
			
			PreparedStatement ps = this.getDbKanino().prepareStatement(sql);
			ps.setMaxRows(1);
			
			ps.setInt(1, idEndereco);
			
			result = ps.executeUpdate();
		} catch (SQLException e) {
			if(e.getErrorCode() == 50000)
			{
				result = -1;
			}else if(e.getErrorCode() == 547)
			{
				result = -2;
			}
			e.printStackTrace();
		} finally {
			try{
				if(this.getDbKanino() != null){
					this.getDbKanino().close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}
}
