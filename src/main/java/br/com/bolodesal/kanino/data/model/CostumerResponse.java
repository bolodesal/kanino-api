package br.com.bolodesal.kanino.data.model;

public class CostumerResponse {

	public CustomerSaved customer;
	
	public String message;
	
	public CostumerResponse(CustomerSaved customer, String message){
		this.customer = customer;
		this.message = message;
	}
	
}