package br.com.bolodesal.kanino.domain;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.servlet.ServletContext;

import org.apache.commons.io.IOUtils;

import br.com.bolodesal.kanino.data.entity.Product;
import br.com.bolodesal.kanino.data.model.ProductNameSearch;
import br.com.bolodesal.kanino.data.model.ProductsPaginated;
import br.com.bolodesal.kanino.data.repository.ProductRepository;
import br.com.bolodesal.kanino.domain.util.Image;
import br.com.bolodesal.kanino.exception.DataNotFoundException;

public class SearchDomain {
	
	private ServletContext context;
		
	ProductRepository productRepository = new ProductRepository();
	
	private String pathImage = "/WEB-INF/images/products/default";

	public void setContext(ServletContext context){
		this.context = context;
	}
	
	public ProductsPaginated<Product> getProducts(String search, int pageNumber, int itensPerPage, int imageWidth, int imageHeight){
		if(pageNumber <= 0) pageNumber = 1;
		if(itensPerPage <= 0) itensPerPage = 1;
		
		ProductsPaginated<Product> productsPaginated = productRepository.getAllPaginatedBySearch(search, pageNumber, itensPerPage);
		if(productsPaginated.produtos == null || productsPaginated.produtos.size() == 0){
			throw new DataNotFoundException("Produtos n�o encontrado.");
		}
		
		for(Product product : productsPaginated.produtos){
			InputStream resourceContent;
			try{
				//resize product image						
					if(product.getImagem() == null || product.getImagem().length == 0)
					{
						try {
							resourceContent = context.getResourceAsStream(String.format("%s/default_product_image.jpg", this.pathImage));
							product.setImagem(IOUtils.toByteArray(resourceContent));
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					BufferedImage image = Image.convertByteArrayToImage(product.getImagem());
					image = Image.resizeImage(image, imageWidth, imageHeight);		 
					product.setImagem(Image.convertImageToByteArray(image));
					//ImageIO.write(image, "jpg", new File("C:\\tomcat7\\webapps\\abc.jpg"));
			} catch(IOException e){
				e.printStackTrace();
			} catch(Exception e){				
				try {
					resourceContent = context.getResourceAsStream(String.format("%s/default_product_image.jpg", this.pathImage));
					product.setImagem(IOUtils.toByteArray(resourceContent));
					BufferedImage image = Image.convertByteArrayToImage(product.getImagem());
					image = Image.resizeImage(image, imageWidth, imageHeight);		 
					product.setImagem(Image.convertImageToByteArray(image));
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}	
		
		return productsPaginated;
	}
	
	public ArrayList<ProductNameSearch> getAllProductsName(){
		ArrayList<ProductNameSearch> products = productRepository.getAllProductsName();
		if(products == null || products.size() == 0){
			throw new DataNotFoundException("Produtos n�o encontrado.");
		}
				
		return products;
	}
}