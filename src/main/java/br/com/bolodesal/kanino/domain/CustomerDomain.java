package br.com.bolodesal.kanino.domain;

import java.util.ArrayList;

import com.sun.jersey.api.client.ClientResponse.Status;

import br.com.bolodesal.kanino.data.entity.Customer;
import br.com.bolodesal.kanino.data.model.Auth;
import br.com.bolodesal.kanino.data.model.AuthResponse;
import br.com.bolodesal.kanino.data.model.CostumerResponse;
import br.com.bolodesal.kanino.data.model.CustomerAddress;
import br.com.bolodesal.kanino.data.model.CustomerSaved;
import br.com.bolodesal.kanino.data.model.GlobalResponse;
import br.com.bolodesal.kanino.data.model.Message;
import br.com.bolodesal.kanino.data.model.Error;
import br.com.bolodesal.kanino.data.repository.CustomerRepository;
import br.com.bolodesal.kanino.exception.DataNotFoundException;
import br.com.bolodesal.kanino.domain.util.Validation;

public class CustomerDomain {	
	
	private CustomerRepository customerRepository = new CustomerRepository();
			
	public Customer getCustomer(int id){
		Customer customer = customerRepository.getById(id);
		
		if (customer == null){
			throw new DataNotFoundException("Cliente n�o encontrado.");
		}
		
		return customer;
	}
	
	public GlobalResponse<?> hasCustomerByEmail(String emailCliente, int idUser){
		
		Validation validate = new Validation();
		
		validate.isValidEmail(emailCliente, "E-mail inv�lido.");
		
		if(!validate.hasErrors()){
			Customer customer = customerRepository.getByEmail(emailCliente);
			
			if (customer == null || (customer.getIdCliente() == idUser && idUser != 0)){				
				return new GlobalResponse<Message>(Status.OK, new Message("E-mail dispo�vel."));			
			}
			validate.addMessage("J� existe um cliente cadastrado com este e-mail.");
			return new GlobalResponse<Error<?>>(Status.CONFLICT, new Error<ArrayList<String>>(validate.getMessages()));	
		}
		return new GlobalResponse<Error<?>>(Status.BAD_REQUEST, new Error<ArrayList<String>>(validate.getMessages()));	
	}
	
	public GlobalResponse<?> hasCustomerByCPF(String CPF){
		
		Validation validate = new Validation();
		
		validate.isValidCPF(CPF, "E-mail inv�lido.");
		
		if(!validate.hasErrors()){
			Customer customer = customerRepository.getByCPF(CPF);
			
			if (customer == null){
				return new GlobalResponse<Message>(Status.OK, new Message("CPF dispon�vel."));			
			}
			validate.addMessage("J� existe um cliente cadastrado com este CPF.");			
			return new GlobalResponse<Error<?>>(Status.CONFLICT, new Error<ArrayList<String>>(validate.getMessages()));	
		}
		return new GlobalResponse<Error<?>>(Status.BAD_REQUEST, new Error<ArrayList<String>>(validate.getMessages()));
	}
	
	public GlobalResponse<?> authCustomer(Auth auth){
		
		Validation validate = new Validation();
		
		validate.isValidEmail(auth.getEmailCliente(), "E-mail inv�lido.");
		
		if(!validate.isNullOrEmpty(auth.getSenhaCliente(), "Digite uma senha."))
		{
			validate.range(auth.getSenhaCliente().length(), 8, 64, "A senha deve conter entre 8 e 64 caracteres.");
		}
		
		if(!validate.hasErrors())
		{
			AuthResponse authModel = new AuthResponse();
			Customer customer = customerRepository.auth(auth);
			boolean customerAuth = false;
			
			if(customer != null){			
				customerAuth = true;
				authModel.setAuth(customerAuth);
				authModel.setCustomer(customer);
				authModel.setMessage("Usu�rio autenciado com sucesso.");
				
				return new GlobalResponse<AuthResponse>(Status.OK, authModel);
			}
			validate.addMessage("Usu�rio ou senha inv�lidos.");
			return new GlobalResponse<Error<?>>(Status.FORBIDDEN, new Error<ArrayList<String>>(validate.getMessages()));
			
		}
		return new GlobalResponse<Error<?>>(Status.CONFLICT, new Error<ArrayList<String>>(validate.getMessages()));		
	}
	
	public GlobalResponse<?> saveCustomerAddress(CustomerAddress customerAddress){
			
		Validation validate = new Validation();
		
		//Customer
		if(!validate.isNullOrEmpty(customerAddress.cliente.getNomeCompletoCliente(), String.format("O nome n�o pode ser vazio.")))
		{
			validate.minLength(customerAddress.cliente.getNomeCompletoCliente().length(), 2, String.format("O nome deve conter no m�nimo 2 caracters."));
			validate.maxLength(customerAddress.cliente.getNomeCompletoCliente().length(), 100, String.format("O nome deve conter no m�ximo 100 caracters."));
		}
		
		if(validate.isValidEmail(customerAddress.cliente.getEmailCliente(), "E-mail inv�lido."))
		{
			validate.hasCustomerWithEmail(customerAddress.cliente, "J� existe um cliente com o e-mail informado.");
		}
		
		if(!validate.isNullOrEmpty(customerAddress.cliente.getSenhaCliente(), "Digite uma senha."))
		{
			validate.range(customerAddress.cliente.getSenhaCliente().length(), 8, 64, "A senha deve conter entre 8 e 64 caracteres.");
		}
		
		if(!validate.isNullOrEmpty(customerAddress.cliente.getCpfCliente(), "Digite um CPF."))
		{
			if(validate.isValidCPF(customerAddress.cliente.getCpfCliente(), "CPF inv�lido."))
			{
				validate.hasCustomerWithCPF(customerAddress.cliente.getCpfCliente(), "J� existe um cliente com o CPF informado.");
			}
		}
		
		if(!validate.isNullOrEmpty(customerAddress.cliente.getCelularCliente(), "Digite um n�mero de celular."))
		{
			validate.range(customerAddress.cliente.getCelularCliente().length(), 11, 12, String.format("O n�mero do celular � inv�lido."));
		}
		
		if(!validate.isNullOrEmpty(customerAddress.cliente.getTelComercialCliente(), ""))
		{
			validate.range(customerAddress.cliente.getTelComercialCliente().length(), 10, 11, "O n�mero do telefone comercial � inv�lido.");
		}
		
		if(!validate.isNullOrEmpty(customerAddress.cliente.getTelResidencialCliente(), ""))
		{
			validate.range(customerAddress.cliente.getTelResidencialCliente().length(), 10, 11, "O n�mero do telefone residencial � inv�lido.");
		}
		
		validate.isValidDate(customerAddress.cliente.getDtNascCliente(), "Data inv�lida. Verifique sua data de nascimento.");
		
		//Address
		if(!validate.isNullOrEmpty(customerAddress.endereco.getNomeEndereco(), "O nome do endere�o n�o pode ser vazio."))
		{
			validate.maxLength(customerAddress.endereco.getNomeEndereco().length(), 50, "O nome do endere�o deve conter no m�ximo 50 caracteres.");
		}
		
		if(!validate.isNullOrEmpty(customerAddress.endereco.getLogradouroEndereco(), "O logradouro n�o pode ser vazio."))
		{
			validate.maxLength(customerAddress.endereco.getLogradouroEndereco().length(), 100, "O logradouro deve conter no m�ximo 100 caracteres.");
		}
		
		if(!validate.isNullOrEmpty(customerAddress.endereco.getNumeroEndereco(), "O n�mero do endere�o n�o pode ser vazio."))
		{
			validate.maxLength(customerAddress.endereco.getNumeroEndereco().length(), 10, "O n�mero do endere�o deve conter no m�ximo 10 caracteres.");
		}
		
		if(!validate.isNullOrEmpty(customerAddress.endereco.getCepEndereco(), "O n�mero do endere�o n�o pode ser vazio."))
		{
			if(validate.hasOnlyNumbers(customerAddress.endereco.getCepEndereco(), "CEP inv�lido. Digite apenas n�meros."))
			{
				validate.maxLength(customerAddress.endereco.getCepEndereco().length(), 10, "O n�mero do endere�o deve conter no m�ximo 9 caracteres.");
			}			
		}
		
		if(validate.notNullOrEmpty(customerAddress.endereco.getComplementoEndereco(), ""))
		{
			validate.maxLength(customerAddress.endereco.getComplementoEndereco().length(), 10, "O complemento do endere�o deve conter no m�ximo 10 caracteres.");
		}
		
		if(!validate.isNullOrEmpty(customerAddress.endereco.getCidadeEndereco(), "A cidade do endere�o n�o pode ser vazio."))
		{
			validate.maxLength(customerAddress.endereco.getCidadeEndereco().length(), 50, "A cidade do endere�o deve conter no m�ximo 50 caracteres.");
		}
		
		if(!validate.isNullOrEmpty(customerAddress.endereco.getPaisEndereco(), "O pa�s n�o pode ser vazio."))
		{
			if(validate.hasOnlyLetters(customerAddress.endereco.getPaisEndereco(), "O pa�s deve conter apenas letras."))
			{
				validate.maxLength(customerAddress.endereco.getPaisEndereco().length(), 50, "O pa�s deve conter no m�ximo 50 caracteres.");
			}
		}
		
		if(!validate.isNullOrEmpty(customerAddress.endereco.getUfEndereco(), "A UF do endere�o n�o pode ser vazio."))
		{
			validate.range(customerAddress.endereco.getUfEndereco().length(), 2, 2, "A UF deve conter 2 caracteres.");
		}
		
		if(!validate.hasErrors())
		{
			int result = customerRepository.saveCustomerAddress(customerAddress);
			
			if (result == 0){
				ArrayList<String> errors = new ArrayList<String>();
				errors.add("Ops! Ocorreu um erro, n�o foi poss�vel cadastrar o cliente.");
				return new GlobalResponse<Error<?>>(Status.INTERNAL_SERVER_ERROR, new Error<ArrayList<String>>(errors));
			}
			CustomerSaved costumerSaved = new CustomerSaved();
			costumerSaved.setIdCliente(result);
			return new GlobalResponse<CostumerResponse>(Status.OK, new CostumerResponse(costumerSaved, "Cliente cadastrado com sucesso!"));
		}

		return new GlobalResponse<Error<?>>(Status.CONFLICT, new Error<ArrayList<String>>(validate.getMessages()));
		
	}
	
	public GlobalResponse<?> saveCustomer(Customer customer){
		
		Validation validate = new Validation();
		
		//Customer
		if(!validate.isNullOrEmpty(customer.getNomeCompletoCliente(), String.format("O nome n�o pode ser vazio.")))
		{
			validate.minLength(customer.getNomeCompletoCliente().length(), 2, String.format("O nome deve conter no m�nimo 2 caracters."));
			validate.maxLength(customer.getNomeCompletoCliente().length(), 100, String.format("O nome deve conter no m�ximo 100 caracters."));
		}
		
		if(validate.isValidEmail(customer.getEmailCliente(), "E-mail inv�lido."))
		{
			validate.hasCustomerWithEmail(customer, "J� existe um cliente com o e-mail informado.");
		}
		
		if(validate.isNullOrEmpty(customer.getSenhaCliente(), "Digite uma senha."))
		{
			validate.range(customer.getSenhaCliente().length(), 8, 64, "A senha deve conter entre 8 e 64 caracteres.");
		}
		
		if(!validate.isNullOrEmpty(customer.getCpfCliente(), "Digite um CPF."))
		{
			if(validate.isValidCPF(customer.getCpfCliente(), "CPF inv�lido."))
			{
				validate.hasCustomerWithCPF(customer.getCpfCliente(), "J� existe um cliente com o CPF informado.");
			}
		}
		
		if(!validate.isNullOrEmpty(customer.getCelularCliente(), "Digite um n�mero de celular."))
		{
			validate.range(customer.getCelularCliente().length(), 11, 12, String.format("O n�mero do celular � inv�lido."));
		}
		
		if(!validate.isNullOrEmpty(customer.getTelComercialCliente(), ""))
		{
			validate.range(customer.getTelComercialCliente().length(), 10, 11, "O n�mero do telefone comercial � inv�lido.");
		}
		
		if(!validate.isNullOrEmpty(customer.getTelResidencialCliente(), ""))
		{
			validate.range(customer.getTelResidencialCliente().length(), 10, 11, "O n�mero do telefone residencial � inv�lido.");
		}
		
		validate.isValidDate(customer.getDtNascCliente(), "Data inv�lida.");
		
		if(!validate.hasErrors())
		{
			int result = customerRepository.save(customer);
			
			if (result == 0){
				ArrayList<String> errors = new ArrayList<String>();
				errors.add("Ops! Ocorreu um erro, n�o foi poss�vel cadastrar o cliente.");
				return new GlobalResponse<Error<?>>(Status.INTERNAL_SERVER_ERROR, new Error<ArrayList<String>>(errors));
			}
			CustomerSaved costumerSaved = new CustomerSaved();
			costumerSaved.setIdCliente(result);
			return new GlobalResponse<CostumerResponse>(Status.OK, new CostumerResponse(costumerSaved, "Cliente cadastrado com sucesso!"));
		}

		return new GlobalResponse<Error<?>>(Status.CONFLICT, new Error<ArrayList<String>>(validate.getMessages()));
		
	}
	
	public GlobalResponse<?> updateCustomer(Customer customer){
		
		Validation validate = new Validation();
		
		if(customer.getIdCliente() > 0)
		{
			if(!validate.isNullOrEmpty(customer.getNomeCompletoCliente(), String.format("O nome n�o pode ser vazio.")))
			{
				validate.minLength(customer.getNomeCompletoCliente().length(), 2, String.format("O nome deve conter no m�nimo 2 caracters."));
				validate.maxLength(customer.getNomeCompletoCliente().length(), 100, String.format("O nome deve conter no m�ximo 100 caracters."));
			}
			
			if(validate.isValidEmail(customer.getEmailCliente(), "E-mail inv�lido."))
			{
				validate.hasCustomerWithEmail(customer, "J� existe um cliente com o e-mail informado.");
			}
			
			if(!validate.isNullOrEmpty(customer.getSenhaCliente(), ""))
			{
				validate.range(customer.getSenhaCliente().length(), 8, 64, "A senha deve conter entre 8 e 64 caracteres.");
			}
			
			validate.notNullOrEmpty(customer.getCpfCliente(), "N�o � permitido mudan�a de CPF.");
			
			if(!validate.isNullOrEmpty(customer.getCelularCliente(), "Digite um n�mero de celular."))
			{
				validate.range(customer.getCelularCliente().length(), 11, 12, String.format("O n�mero do celular � inv�lido."));
			}
			
			if(!validate.isNullOrEmpty(customer.getTelComercialCliente(), ""))
			{
				validate.range(customer.getTelComercialCliente().length(), 10, 11, "O n�mero do telefone comercial � inv�lido.");
			}
			
			if(!validate.isNullOrEmpty(customer.getTelResidencialCliente(), ""))
			{
				validate.range(customer.getTelResidencialCliente().length(), 10, 11, "O n�mero do telefone residencial � inv�lido.");
			}
			
			validate.isValidDate(customer.getDtNascCliente(), "Data inv�lida.");
			
			if(!validate.hasErrors())
			{
				int result = customerRepository.update(customer);
				ArrayList<String> errors = new ArrayList<String>();
				Status status = Status.SERVICE_UNAVAILABLE;
								
				if(result <= 0)
				{
					if (result == 0)
					{
						status = Status.INTERNAL_SERVER_ERROR;
						errors.add("Ops! Ocorreu um erro, n�o foi poss�vel atualizar os dados do cliente.");						
					}else if(result == -1)
					{
						status = Status.FORBIDDEN;
						errors.add("Voc� n�o tem permiss�o para atualizar este usu�rio.");
					}
					return new GlobalResponse<Error<?>>(status, new Error<ArrayList<String>>(errors));
				}
				
				return new GlobalResponse<Message>(Status.OK, new Message("Dados atualizado com sucesso!"));
			}

			return new GlobalResponse<Error<?>>(Status.CONFLICT, new Error<ArrayList<String>>(validate.getMessages()));
		}
		validate.addMessage("Cliente n�o encontrado.");
		return new GlobalResponse<Error<?>>(Status.BAD_REQUEST, new Error<ArrayList<String>>(validate.getMessages()));		
	}
}
