package br.com.bolodesal.kanino.domain;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.apache.commons.io.IOUtils;

import javax.servlet.ServletContext;

import br.com.bolodesal.kanino.data.entity.Product;
import br.com.bolodesal.kanino.data.model.ProductsPaginated;
import br.com.bolodesal.kanino.data.repository.ProductRepository;
import br.com.bolodesal.kanino.domain.util.Image;
import br.com.bolodesal.kanino.exception.DataNotFoundException;;;

public class ProductDomain {
	
	private ServletContext context;
	
	private ProductRepository productRepository = new ProductRepository();
	
	private String pathImage = "/WEB-INF/images/products/default";
	
	public void setContext(ServletContext context){
		this.context = context;
	}
	
	public Product getById(int id, int imageWidth, int imageHeight){
		Product product = productRepository.getById(id);
		
		if(product == null){
			throw new DataNotFoundException("Produto n�o encontrado.");
		}
		
		InputStream resourceContent;
		
		try{
			if(product.getImagem() == null || product.getImagem().length == 0)
			{
				try {
					resourceContent = context.getResourceAsStream(String.format("%s/default_product_image.jpg", this.pathImage));
					product.setImagem(IOUtils.toByteArray(resourceContent));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			BufferedImage image = Image.convertByteArrayToImage(product.getImagem());
			image = Image.resizeImage(image, imageWidth, imageHeight);		 
			product.setImagem(Image.convertImageToByteArray(image));
			//ImageIO.write(image, "jpg", new File("C:\\tomcat7\\webapps\\abc.jpg"));			
			
		} catch(IOException e){
			e.printStackTrace();
		} catch(Exception e){			
			try {
				resourceContent = context.getResourceAsStream(String.format("%s/default_product_image.jpg", this.pathImage));
				product.setImagem(IOUtils.toByteArray(resourceContent));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		return product;
		
	}
	
	public ArrayList<Product> getByCategoryId(int categoryId, int qtdItens, int lastProductOrderId, int imageWidth, int imageHeight){	
		ArrayList<Product> products = productRepository.getByCategoryId(categoryId, qtdItens, lastProductOrderId);
		if(products == null)
		{
			throw new DataNotFoundException("Produto n�o encontrado.");
		}
		for(Product product : products){
			InputStream resourceContent;
			try{			
				//resize product image		
				if(product.getImagem() == null || product.getImagem().length == 0)
				{					
					try {
						resourceContent = context.getResourceAsStream(String.format("%s/default_product_image.jpg", this.pathImage));
						product.setImagem(IOUtils.toByteArray(resourceContent));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				BufferedImage image = Image.convertByteArrayToImage(product.getImagem());
				image = Image.resizeImage(image, imageWidth, imageHeight);		 
				product.setImagem(Image.convertImageToByteArray(image));
				//ImageIO.write(image, "jpg", new File("C:\\tomcat7\\webapps\\abc.jpg"));
				
			} catch(IOException e){
				e.printStackTrace();
			} catch(Exception e){				
				try {
					resourceContent = context.getResourceAsStream(String.format("%s/default_product_image.jpg", this.pathImage));				
					product.setImagem(IOUtils.toByteArray(resourceContent));
					BufferedImage image = Image.convertByteArrayToImage(product.getImagem());
					image = Image.resizeImage(image, imageWidth, imageHeight);		 
					product.setImagem(Image.convertImageToByteArray(image));
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}				
			}
		}	
		
		return products;
	}
	
	public ProductsPaginated<Product> getProductsPaginatedByCategoryId(int categoryId, int pageNumber, int itensPerPage, int imageWidth, int imageHeight){	
		if(pageNumber <= 0) pageNumber = 1;
		if(itensPerPage <= 0) itensPerPage = 1;
		
		ProductsPaginated<Product> productsPaginated = productRepository.getProductsPaginatedByCategoryId(categoryId, pageNumber, itensPerPage);
		if(productsPaginated.produtos == null || productsPaginated.produtos.size() == 0){
			throw new DataNotFoundException("Produto n�o encontrado.");
		}
		for(Product product : productsPaginated.produtos){
			InputStream resourceContent;
			try{
				//resize product image				
					if(product.getImagem() == null || product.getImagem().length == 0)
					{						
						try {
							resourceContent = context.getResourceAsStream(String.format("%s/default_product_image.jpg", this.pathImage));
							product.setImagem(IOUtils.toByteArray(resourceContent));
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					BufferedImage image = Image.convertByteArrayToImage(product.getImagem());
					image = Image.resizeImage(image, imageWidth, imageHeight);		 
					product.setImagem(Image.convertImageToByteArray(image));
					//ImageIO.write(image, "jpg", new File("C:\\tomcat7\\webapps\\abc.jpg"));
			} catch(IOException e){
				e.printStackTrace();
			} catch(Exception e){				
				try {
					resourceContent = context.getResourceAsStream(String.format("%s/default_product_image.jpg", this.pathImage));
					product.setImagem(IOUtils.toByteArray(resourceContent));
					BufferedImage image = Image.convertByteArrayToImage(product.getImagem());
					image = Image.resizeImage(image, imageWidth, imageHeight);		 
					product.setImagem(Image.convertImageToByteArray(image));
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
		
		
		return productsPaginated;
	}
		
	public ProductsPaginated<Product> getProductsPaginated(int pageNumber, int itensPerPage, int imageWidth, int imageHeight){
		if(pageNumber <= 0) pageNumber = 1;
		if(itensPerPage <= 0) itensPerPage = 1;
		
		ProductsPaginated<Product> productsPaginated = productRepository.getAllPaginated(pageNumber, itensPerPage);
		if(productsPaginated.produtos == null || productsPaginated.produtos.size() == 0){
			throw new DataNotFoundException("Produto n�o encontrado.");
		}
		
		
			//resize product image			
			for(Product product : productsPaginated.produtos){
				InputStream resourceContent;
				try{
					if(product.getImagem() == null || product.getImagem().length == 0)
					{
						try {
							resourceContent = context.getResourceAsStream(String.format("%s/default_product_image.jpg", this.pathImage));
							product.setImagem(IOUtils.toByteArray(resourceContent));
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					BufferedImage image = Image.convertByteArrayToImage(product.getImagem());
					image = Image.resizeImage(image, imageWidth, imageHeight);		 
					product.setImagem(Image.convertImageToByteArray(image));
					//ImageIO.write(image, "jpg", new File("C:\\tomcat7\\webapps\\abc.jpg"));				
				} catch(IOException e){
					e.printStackTrace();
				} catch(Exception e){
					try {
						resourceContent = context.getResourceAsStream(String.format("%s/default_product_image.jpg", this.pathImage));
						product.setImagem(IOUtils.toByteArray(resourceContent));
						BufferedImage image = Image.convertByteArrayToImage(product.getImagem());
						image = Image.resizeImage(image, imageWidth, imageHeight);		 
						product.setImagem(Image.convertImageToByteArray(image));
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}		
		
		return productsPaginated;
	}	
}
