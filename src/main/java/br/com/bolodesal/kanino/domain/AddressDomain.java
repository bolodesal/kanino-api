package br.com.bolodesal.kanino.domain;

import java.util.ArrayList;

import com.sun.jersey.api.client.ClientResponse.Status;

import br.com.bolodesal.kanino.data.entity.Address;
import br.com.bolodesal.kanino.data.model.Error;
import br.com.bolodesal.kanino.data.model.GlobalResponse;
import br.com.bolodesal.kanino.data.model.Message;
import br.com.bolodesal.kanino.data.repository.AddressRepository;
import br.com.bolodesal.kanino.domain.util.Validation;
import br.com.bolodesal.kanino.exception.DataNotFoundException;

public class AddressDomain {
	
	private AddressRepository addressRepository = new AddressRepository();
	
	public Address getAddress(int customerId){
		Address address = addressRepository.getAddress(customerId);
		
		if(address == null)
		{
			throw new DataNotFoundException("Endere�o n�o encontrado.");
		}
		return address;
	}
	
	public ArrayList<Address> getAddressesByCostumerId(int customerId){
		ArrayList<Address> addresses = addressRepository.getAddressesByCostumerId(customerId);
		
		if(addresses == null || addresses.isEmpty())
		{
			throw new DataNotFoundException("Endere�o(s) n�o encontrado(s).");
		}
		return addresses;
	}

	public GlobalResponse<?> saveAddress(Address address){
		
		Validation validate = new Validation();
		
		if(!validate.isNullOrEmpty(address.getNomeEndereco(), "O nome do endere�o n�o pode ser vazio."))
		{
			validate.maxLength(address.getNomeEndereco().length(), 50, "O nome do endere�o deve conter no m�ximo 50 caracteres.");
		}
		
		if(!validate.isNullOrEmpty(address.getLogradouroEndereco(), "O logradouro n�o pode ser vazio."))
		{
			validate.maxLength(address.getLogradouroEndereco().length(), 100, "O logradouro deve conter no m�ximo 100 caracteres.");
		}
		
		if(!validate.isNullOrEmpty(address.getNumeroEndereco(), "O n�mero do endere�o n�o pode ser vazio."))
		{
			validate.maxLength(address.getNumeroEndereco().length(), 10, "O n�mero do endere�o deve conter no m�ximo 10 caracteres.");
		}
		
		if(!validate.isNullOrEmpty(address.getCepEndereco(), "O n�mero do endere�o n�o pode ser vazio."))
		{
			if(validate.hasOnlyNumbers(address.getCepEndereco(), "CEP inv�lido. Digite apenas n�meros."))
			{
				validate.maxLength(address.getCepEndereco().length(), 10, "O n�mero do endere�o deve conter no m�ximo 9 caracteres.");
			}			
		}
		
		if(validate.notNullOrEmpty(address.getComplementoEndereco(), ""))
		{
			validate.maxLength(address.getComplementoEndereco().length(), 10, "O complemento do endere�o deve conter no m�ximo 10 caracteres.");
		}
		
		if(!validate.isNullOrEmpty(address.getCidadeEndereco(), "A cidade do endere�o n�o pode ser vazio."))
		{
			validate.maxLength(address.getCidadeEndereco().length(), 50, "A cidade do endere�o deve conter no m�ximo 50 caracteres.");
		}
		
		if(!validate.isNullOrEmpty(address.getPaisEndereco(), "O pa�s n�o pode ser vazio."))
		{
			if(validate.hasOnlyLetters(address.getPaisEndereco(), "O pa�s deve conter apenas letras."))
			{
				validate.maxLength(address.getPaisEndereco().length(), 50, "O pa�s deve conter no m�ximo 50 caracteres.");
			}
		}
		
		if(!validate.isNullOrEmpty(address.getUfEndereco(), "A UF do endere�o n�o pode ser vazio."))
		{
			validate.range(address.getUfEndereco().length(), 2, 2, "A UF deve conter 2 caracteres.");
		}
		
		if(!validate.hasErrors())
		{
			int result = addressRepository.save(address);
			
			if (result == 0){
				validate.addMessage("Ops! Ocorreu um erro, n�o foi poss�vel cadastrar o endere�o.");
				return new GlobalResponse<Error<?>>(Status.INTERNAL_SERVER_ERROR, new Error<ArrayList<String>>(validate.getMessages()));
			}
			
			return new GlobalResponse<Message>(Status.OK, new Message("Endere�o cadastrado com sucesso."));
		}		
		
		return new GlobalResponse<Error<?>>(Status.CONFLICT, new Error<ArrayList<String>>(validate.getMessages()));
	}
	
	public GlobalResponse<?> updateAddress(Address address){
		
		Validation validate = new Validation();
		
		if(!validate.isNullOrEmpty(address.getNomeEndereco(), "O nome do endere�o n�o pode ser vazio."))
		{
			validate.maxLength(address.getNomeEndereco().length(), 50, "O nome do endere�o deve conter no m�ximo 50 caracteres.");
		}
		
		if(!validate.isNullOrEmpty(address.getLogradouroEndereco(), "O logradouro n�o pode ser vazio."))
		{
			validate.maxLength(address.getLogradouroEndereco().length(), 100, "O logradouro deve conter no m�ximo 100 caracteres.");
		}
		
		if(!validate.isNullOrEmpty(address.getNumeroEndereco(), "O n�mero do endere�o n�o pode ser vazio."))
		{
			validate.maxLength(address.getNumeroEndereco().length(), 10, "O n�mero do endere�o deve conter no m�ximo 10 caracteres.");
		}
		
		if(!validate.isNullOrEmpty(address.getCepEndereco(), "O CEP do endere�o n�o pode ser vazio."))
		{
			if(validate.hasOnlyNumbers(address.getCepEndereco(), "CEP inv�lido. Digite apenas n�meros."))
			{
				validate.maxLength(address.getCepEndereco().length(), 10, "O n�mero do endere�o deve conter no m�ximo 9 caracteres.");
			}			
		}
		
		if(validate.notNullOrEmpty(address.getComplementoEndereco(), ""))
		{
			validate.maxLength(address.getComplementoEndereco().length(), 10, "O complemento do endere�o deve conter no m�ximo 10 caracteres.");
		}
		
		if(!validate.isNullOrEmpty(address.getCidadeEndereco(), "A cidade do endere�o n�o pode ser vazio."))
		{
			validate.maxLength(address.getCidadeEndereco().length(), 50, "A cidade do endere�o deve conter no m�ximo 50 caracteres.");
		}
		
		if(!validate.isNullOrEmpty(address.getPaisEndereco(), "O pa�s n�o pode ser vazio."))
		{
			if(validate.hasOnlyLetters(address.getPaisEndereco(), "O pa�s deve conter apenas letras."))
			{
				validate.maxLength(address.getPaisEndereco().length(), 50, "O pa�s deve conter no m�ximo 50 caracteres.");
			}
		}
		
		if(!validate.isNullOrEmpty(address.getUfEndereco(), "A UF do endere�o n�o pode ser vazio."))
		{
			validate.range(address.getUfEndereco().length(), 2, 2, "A UF deve conter 2 caracteres.");
		}
		
		if(!validate.hasErrors())
		{
			int result = addressRepository.update(address);
			ArrayList<String> errors = new ArrayList<String>();
			
			if(result <= 0)
			{
				if (result == 0)
				{
					errors.add("Ops! Ocorreu um erro, n�o foi poss�vel atualizar os dados do endere�o.");						
				}
				else if(result == -1)
				{
					errors.add("Voc� n�o tem permiss�o para atualizar este endere�o.");
				}
				return new GlobalResponse<Error<?>>(Status.FORBIDDEN, new Error<ArrayList<String>>(errors));
			}
			return new GlobalResponse<Message>(Status.OK, new Message("Endere�o atualizado com sucesso."));
		}
		return new GlobalResponse<Error<?>>(Status.CONFLICT, new Error<ArrayList<String>>(validate.getMessages()));
	}
	
	public GlobalResponse<?> deleteAddress(int addressId){
		
		Validation validate = new Validation();
		
		if(addressId > 0)
		{
			int result = addressRepository.delete(addressId);
			ArrayList<String> errors = new ArrayList<String>();
			Status status = Status.SERVICE_UNAVAILABLE;
			
			if(result <= 0)
			{
				if (result == 0)
				{
					status = Status.INTERNAL_SERVER_ERROR;
					errors.add("Ops! Ocorreu um erro, n�o foi poss�vel remover o endere�o.");						
				}else if(result == -1)
				{
					status = Status.FORBIDDEN;
					errors.add("Voc� n�o tem permiss�o para remover este endere�o.");
				}
				else if(result == -2)
				{
					status = Status.FORBIDDEN;
					errors.add("Este endere�o n�o pode ser removido pois h� pedidos vinculados ao mesmo.");
				}
				return new GlobalResponse<Error<?>>(status, new Error<ArrayList<String>>(errors));
			}
			return new GlobalResponse<Message>(Status.OK, new Message("Endere�o removido com sucesso."));
		}
		validate.addMessage("Endere�o n�o encontrado.");
		return new GlobalResponse<Error<?>>(Status.CONFLICT, new Error<ArrayList<String>>(validate.getMessages()));
	}
}
