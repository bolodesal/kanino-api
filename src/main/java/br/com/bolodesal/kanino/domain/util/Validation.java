package br.com.bolodesal.kanino.domain.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import org.apache.commons.validator.routines.EmailValidator;

import br.com.bolodesal.kanino.data.entity.Customer;
import br.com.bolodesal.kanino.data.entity.OrderItem;
import br.com.bolodesal.kanino.data.repository.CustomerRepository;
import br.com.bolodesal.kanino.data.repository.OrderRepository;

public class Validation {
		
	private ArrayList<String> messages = new ArrayList<String>();
		
	public ArrayList<String> getMessages(){
		return this.messages;
	}
	
	public void addMessage(String message){
		this.messages.add(message);
	}
		
	public boolean hasErrors() {
		this.messages.removeAll(Arrays.asList(null, ""));
		return !this.messages.isEmpty();
	}
	
	public boolean isNullOrEmpty(String text, String message){
		if(text == null || text.trim().length() == 0){
			this.messages.add(message);
			return true;
		}		
		return false;
	}
	
	public boolean isZero(int num, String message){
		if(num == 0){
			this.messages.add(message);
			return true;
		}		
		return false;
	}
	
	public boolean notNullOrEmpty(String text, String message){
		if(text != null && text.trim().length() > 0){
			this.messages.add(message);
			return true;
		}		
		return false;
	}
	
	public boolean hasOnlyLetters(String text, String message){
		if(!text.matches(".*[0-9].*"))
		{
			return true;
		}
		this.messages.add(message);
		return false;
	}
	
	public boolean minLength(int value, int min,  String message){
		if(value >= min)
		{
			return true;
		}
		this.messages.add(message);
		return false;
	}
	
	public boolean maxLength(int value, int max,  String message){
		if(value <= max)
		{
			return true;
		}
		this.messages.add(message);
		return false;
	}

	public boolean range(double value, double min, double max, String message){
		if(value >= min && value <= max)
		{
			return true;
		}
		this.messages.add(message);
		return false;
	}
	
	public boolean range(double value, double min, String message){
		if(value >= min)
		{
			return true;
		}
		this.messages.add(message);
		return false;
	}
	
	public boolean isValidCPF(String cpf, String message){
		String base = "000000000";  
		String digitos = "00";  
		if (cpf.length() <= 11) {
			if (cpf.length() < 11) {
				cpf = base.substring(0, 11 - cpf.length()) + cpf;
				base = cpf.substring(0, 9);
			}
			base = cpf.substring(0, 9);
			digitos = cpf.substring(9, 11);
			int soma = 0, mult = 11;
			int[] var = new int[11];
			// Recebe os n�meros e realiza a multiplica��o e soma.
			for (int i = 0; i < 9; i++) {
				var[i] = Integer.parseInt("" + cpf.charAt(i));
				if (i < 9)
					soma += (var[i] * --mult);
			}
			// Cria o primeiro d�gito verificador.
			int resto = soma % 11;
			if (resto < 2) {
				var[9] = 0;
			} else {
				var[9] = 11 - resto;
			}
			// Reinicia os valores.
			soma = 0;
			mult = 11;
			// Realiza a multiplica��o e soma do segundo d�gito.
			for (int i = 0; i < 10; i++)
				soma += var[i] * mult--;
			// Cria o segundo d�gito verificador.
			resto = soma % 11;
			if (resto < 2) {
				var[10] = 0;
			} else {
				var[10] = 11 - resto;
			}
			if ((digitos.substring(0, 1).equalsIgnoreCase(new Integer(var[9]).toString()))
					&& (digitos.substring(1, 2).equalsIgnoreCase(new Integer(var[10]).toString()))) {
				return true;
			}
		}
		this.messages.add(message);
		return false;
	}
	
	public boolean isValidEmail(String emailCliente, String message){
		
		EmailValidator validator = EmailValidator.getInstance();
		if(validator.isValid(emailCliente))
		{
			return true;
		}
		this.messages.add(message);
		return false;
	}
	
	public boolean isValidDate(Date date, String message)
	{
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
			String currentDate = sdf.format(new Date());
			Date minDate = sdf.parse("31121899");
			Date maxDate = sdf.parse(currentDate);
			if(date.after(minDate) && date.before(maxDate))
			{
				return true;
			}
		} catch(ParseException ex) {
			this.messages.add(message);
			return false;
		}
		this.messages.add(message);
		return false;
	}
	
	public boolean hasOnlyNumbers(String text, String message)
	{
		if(text.matches("\\d+"))
		{
			return true;
		}
		this.messages.add(message);
		return false;
	}
	
	//Customer
	public boolean hasCustomerWithEmail(Customer customer, String message){
		CustomerRepository customerRepository = new CustomerRepository();
		Customer customerByEmail = customerRepository.getByEmail(customer.getEmailCliente());
		if(customerByEmail != null && customer.getIdCliente() != customerByEmail.getIdCliente())
		{
			this.messages.add(message);
			return true;
		}		
		return false;
	}
	
	public boolean hasCustomerWithCPF(String cpf, String message){
		CustomerRepository customerRepository = new CustomerRepository();
		if(customerRepository.getByCPF(cpf) == null)
		{
			return true;
		}
		this.messages.add(message);
		return false;
	}

	//Order
	public boolean isValidTimestamp(Timestamp timestamp, String message){
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String currentDate = sdf.format(new Date());
			Date minDate = sdf.parse("31121899");
			Date maxDate = sdf.parse(currentDate);
			if(timestamp.after(minDate) && timestamp.before(maxDate))
			{
				return true;
			}
		} catch(ParseException ex) {
			this.messages.add(message);
			return false;
		}
		this.messages.add(message);
		return false;
	}
	
	//OrderItem
	public boolean hasInStock(OrderItem orderItem, String message){
		OrderRepository orderRepository = new OrderRepository();
		
		if (orderRepository.verificaQtdEstoque(orderItem.getIdProduto()) > 0){
			return true;			
		} 
		
		this.messages.add(message);
		return false;
		
	}
	
	public boolean hasQty(OrderItem orderItem, String message){
		OrderRepository orderRepository = new OrderRepository();
		
		if (orderRepository.verificaQtdEstoque(orderItem.getIdProduto()) > orderItem.getQtdProduto()){
			return true;
		}
		
		this.messages.add(message);
		return false;
	}
}
