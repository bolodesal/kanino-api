package br.com.bolodesal.kanino.domain;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletContext;

import org.apache.commons.io.IOUtils;

import com.sun.jersey.api.client.ClientResponse.Status;

import br.com.bolodesal.kanino.data.entity.Order;
import br.com.bolodesal.kanino.data.entity.OrderItem;
import br.com.bolodesal.kanino.data.entity.Product;
import br.com.bolodesal.kanino.data.model.GlobalResponse;
import br.com.bolodesal.kanino.data.model.OrderArray;
import br.com.bolodesal.kanino.data.model.OrderResponse;
import br.com.bolodesal.kanino.data.model.OrderSaved;
import br.com.bolodesal.kanino.data.repository.OrderRepository;
import br.com.bolodesal.kanino.data.repository.ProductRepository;
import br.com.bolodesal.kanino.domain.util.Image;
import br.com.bolodesal.kanino.domain.util.Validation;
import br.com.bolodesal.kanino.exception.DataNotFoundException;
import br.com.bolodesal.kanino.data.model.Error;

public class OrderDomain {

	private ServletContext context;
	
	private OrderRepository orderRepository = new OrderRepository();
	
	private String pathImage = "/WEB-INF/images/products/default";
	
	public void setContext(ServletContext context){
		this.context = context;
	}
	
	public Order getOrder(int idPedido){
		Order order = orderRepository.get(idPedido);
		if(order == null)
		{
			throw new DataNotFoundException(String.format("Pedido n�o encontrado.", idPedido));
		}
		return order;
	}
	
	public ArrayList<OrderItem> getOrderDetails(int idPedido, int imageWidth, int imageHeight){
		ArrayList<OrderItem> orderItems = orderRepository.getDetails(idPedido);
		
		if (orderItems == null){
			throw new DataNotFoundException(String.format("Order with id %d not found.", idPedido));
		} 
		
		for(OrderItem orderItem : orderItems){
			try{
				if(orderItem.getImagem() == null || orderItem.getImagem().length == 0)
				{
					InputStream resourceContent = context.getResourceAsStream(String.format("%s/default_orderItem_image.jpg", this.pathImage));
					try {
						orderItem.setImagem(IOUtils.toByteArray(resourceContent));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				BufferedImage image = Image.convertByteArrayToImage(orderItem.getImagem());
				image = Image.resizeImage(image, imageWidth, imageHeight);		 
				orderItem.setImagem(Image.convertImageToByteArray(image));
				//ImageIO.write(image, "jpg", new File("C:\\tomcat7\\webapps\\abc.jpg"));			
				
			} catch(IOException e){
				e.printStackTrace();
			}
		}
		
		return orderItems;
	}
	
	public ArrayList<Order> getOrdersByCustomer(int idCliente){
		ArrayList<Order> orders = orderRepository.getOrdersByCustomer(idCliente);
		
		if (orders == null){
			throw new DataNotFoundException(String.format("Order for customer with id %d not found.", idCliente));
		}
		
		return orders;
	}
		
 	public GlobalResponse<?> saveOrder(OrderArray orderArray) throws SQLException{		
		
		Validation validate = new Validation();
		
		//Order (idStatus, idCliente, idAplicacao, idEndereco, idTipoPagto, dataPedido)
		validate.isZero(orderArray.getOrder().getIdAplicacao(), "Inserir ID da aplica��o utilizada");
		validate.isZero(orderArray.getOrder().getIdCliente(), "Inserir ID do cliente do pedido");
		validate.isZero(orderArray.getOrder().getIdEndereco(), "Inserir ID do endere�o do pedido");
		validate.isZero(orderArray.getOrder().getIdStatus(), "Inserir ID do status do pedido");
		validate.isZero(orderArray.getOrder().getIdTipoPagto(), "Inserir ID do tipo de pagamento do pedido");
		if(validate.isNullOrEmpty(orderArray.getOrder().getDataPedido().toString(), "Inserir data do pedido"))
		{
			validate.isValidDate(orderArray.getOrder().getDataPedido(), "Data inv�lida");
		}
				
		//OrderItens (idProduto,precoVendaItem,qtdProduto)
		
		ProductRepository productRepository = new ProductRepository();;
		for(OrderItem orderItem : orderArray.getOrderItens()){
			Product product = productRepository.getById(orderItem.getIdProduto());
			
			validate.isZero(orderItem.getIdProduto(), "Inserir ID do pedido no produto");
			if(!validate.isZero(orderItem.getQtdProduto(), "Quantidade n�o pode ser zero")){
				if(validate.hasInStock(orderItem, "Produto " + product.getNomeProduto() + " fora de estoque!")){
					validate.hasQty(orderItem, "Produto " + product.getNomeProduto() + " n�o dispon�vel na quantidade desejada!");
				}
			}
			
			if(validate.isZero(orderItem.getPrecoVendaItem().intValue(), "Inserir pre�o do produto"))
			{
				validate.range(orderItem.getPrecoVendaItem(), 0, "Valor n�o pode ser menor que zero");
			}
		}
		
		if(!validate.hasErrors())
		{
			int result = orderRepository.save(orderArray.getOrder(), orderArray.getOrderItens());
			
			if (result == 0){
				ArrayList<String> errors = new ArrayList<String>();
				errors.add("Ops! Ocorreu um erro, n�o foi poss�vel cadastrar o pedido.");
				return new GlobalResponse<Error<?>>(Status.INTERNAL_SERVER_ERROR, new Error<ArrayList<String>>(errors));
			}
			OrderSaved orderSaved = new OrderSaved();
			orderSaved.setIdPedido(result);
			
			return new GlobalResponse<OrderResponse>(Status.OK, new OrderResponse(orderSaved, "Pedido cadastrado com sucesso!"));
		}
		return new GlobalResponse<Error<?>>(Status.CONFLICT, new Error<ArrayList<String>>(validate.getMessages()));
	}
	
	public GlobalResponse<?> updateOrder(Order order){
		
		Validation validate = new Validation();
		
		if(!validate.isZero(order.getIdPedido(), "Inserir ID do pedido")){
			validate.isZero(order.getIdAplicacao(), "Inserir ID da aplica��o utilizada");
			validate.isZero(order.getIdCliente(), "Inserir ID do cliente do pedido");
			validate.isZero(order.getIdEndereco(), "Inserir ID do endere�o do pedido");
			validate.isZero(order.getIdStatus(), "Inserir ID do status do pedido");
			validate.isZero(order.getIdTipoPagto(), "Inserir ID do tipo de pagamento do pedido");
			if(validate.isNullOrEmpty(order.getDataPedido().toString(), "Inserir data do pedido"))
			{
				validate.isValidDate(order.getDataPedido(), "Data inv�lida");
			}
		}
		
		if(!validate.hasErrors())
		{
			Order orderAux = orderRepository.get(order.getIdPedido());
			if(orderAux.getIdStatus() != 5){
			
				int result = orderRepository.update(order);
				
				if (result == 0){
					ArrayList<String> errors = new ArrayList<String>();
					errors.add("Ops! Ocorreu um erro, n�o foi poss�vel atualizar o pedido.");
					return new GlobalResponse<Error<?>>(Status.INTERNAL_SERVER_ERROR, new Error<ArrayList<String>>(errors));
				}
				OrderSaved orderSaved = new OrderSaved();
				orderSaved.setIdPedido(result);
				
				return new GlobalResponse<OrderResponse>(Status.OK, new OrderResponse(orderSaved, "Pedido atualizar com sucesso!"));
			} else {
				ArrayList<String> errors = new ArrayList<>();
				errors.add("N�o � poss�vel alterar o status de um pedido cancelado.");
				return new GlobalResponse<Error<?>>(Status.FORBIDDEN, new Error<ArrayList<String>>(errors));
			}
		}
		return new GlobalResponse<Error<?>>(Status.CONFLICT, new Error<ArrayList<String>>(validate.getMessages()));
	}

}
