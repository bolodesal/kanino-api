package br.com.bolodesal.kanino.domain.util;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Image {
	
	public static BufferedImage convertByteArrayToImage(byte[] image) throws IOException 
	{
		BufferedImage img = ImageIO.read(new ByteArrayInputStream(image));
		
		return img;
	}
	
	public static byte[] convertImageToByteArray(BufferedImage image) throws IOException 
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(image, "jpg", baos);
		
		return baos.toByteArray();
	}
	
	public static BufferedImage getScaledImage(BufferedImage image, int width, int height) throws IOException {
	    int imageWidth  = image.getWidth();
	    int imageHeight = image.getHeight();

	    double scaleX = (double)width/imageWidth;
	    double scaleY = (double)height/imageHeight;
	    AffineTransform scaleTransform = AffineTransform.getScaleInstance(scaleX, scaleY);
	    AffineTransformOp bilinearScaleOp = new AffineTransformOp(scaleTransform, AffineTransformOp.TYPE_BILINEAR);

	    return bilinearScaleOp.filter(image, new BufferedImage(width, height, image.getType()));
	}
	
	public static BufferedImage resizeImage(BufferedImage image, Integer width, Integer height) throws IOException {
            int type = image.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : image.getType();
            if (width == 0) {
            	width = image.getWidth();
            }
            if (height == 0) {
            	height = image.getHeight();
            }
            int fHeight = height;
            int fWidth = width;
            //Work out the resized width/height
            if (image.getHeight() > height || image.getWidth() > width) {
                fHeight = height;
                int wid = width;
                float sum = (float)image.getWidth() / (float)image.getHeight();
                fWidth = Math.round(fHeight * sum);
                if (fWidth > wid) {
                    //rezise again for the width this time
                    fHeight = Math.round(wid/sum);
                    fWidth = wid;
                }
            }
            BufferedImage resizedImage = new BufferedImage(width, height, type);
            Graphics2D g = resizedImage.createGraphics();
            g.setColor(Color.WHITE);
            g.fillRect(0, 0, width, height);
            g.setComposite(AlphaComposite.Src);
            g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g.drawImage(image, (width/2) - (fWidth/2), (height/2) - (fHeight/2), fWidth, fHeight, null);
            g.dispose();
            return resizedImage;
    }	
}