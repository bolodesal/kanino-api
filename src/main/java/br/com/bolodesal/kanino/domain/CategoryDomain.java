package br.com.bolodesal.kanino.domain;

import java.util.ArrayList;

import br.com.bolodesal.kanino.data.entity.Category;
import br.com.bolodesal.kanino.data.repository.CategoryRepository;
import br.com.bolodesal.kanino.exception.DataNotFoundException;

public class CategoryDomain {
	
	CategoryRepository categoryRepository = new CategoryRepository();
	
	public ArrayList<Category> getCategories(){
		ArrayList<Category> categories = categoryRepository.getAll();
		
		if(categories == null)
		{
			throw new DataNotFoundException("No categories found");
		}
		
		return categories;
	}

}
