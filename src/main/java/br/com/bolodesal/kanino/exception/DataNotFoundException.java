package br.com.bolodesal.kanino.exception;

public class DataNotFoundException extends RuntimeException {

	/**
	 * Automatically generated
	 */
	private static final long serialVersionUID = 4726124274047431883L;
	
	public DataNotFoundException(String message){
		super(message);
	}
}
