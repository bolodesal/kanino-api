package br.com.bolodesal.kanino.exception;

import java.util.ArrayList;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.sun.jersey.api.client.ClientResponse.Status;

import br.com.bolodesal.kanino.data.model.GlobalResponse;
import br.com.bolodesal.kanino.data.model.Error;

@Provider
public class DataNotFoundExceptionMapper implements ExceptionMapper<DataNotFoundException> {
	
	@Override
	public Response toResponse(DataNotFoundException exception) {
		ArrayList<String> errors = new ArrayList<String>();
		errors.add(exception.getMessage());
		GlobalResponse<?> error = new GlobalResponse<Error<?>>(Status.NOT_FOUND, new Error<ArrayList<String>>(errors));
		return Response.status(Status.NOT_FOUND).entity(error).build();
	}
}