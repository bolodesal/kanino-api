package br.com.bolodesal.kanino.exception;

import java.util.ArrayList;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.owlike.genson.JsonBindingException;
import com.sun.jersey.api.NotFoundException;
import com.sun.jersey.api.client.ClientResponse.Status;

import br.com.bolodesal.kanino.data.model.Error;
import br.com.bolodesal.kanino.data.model.GlobalResponse;

@Provider
public class WebApplicationExceptionMapper implements ExceptionMapper<WebApplicationException> {

	@Override
	public Response toResponse(WebApplicationException exception) {
		ArrayList<String> errors = new ArrayList<String>();
		Status status = Status.INTERNAL_SERVER_ERROR;
		
		if(exception.getCause() instanceof JsonBindingException)
		{
			status = Status.BAD_REQUEST;
			errors.add("Formato JSON inv�lido.");
		}else if(exception instanceof NotFoundException)
		{
			status = Status.NOT_FOUND;
			errors.add("API n�o encontrada.");
		}else{
			errors.add("Ops! Ocorreu um erro, estamos trabalhando para resolver.");
		}
		
		GlobalResponse<?> error = new GlobalResponse<Error<?>>(status, new Error<ArrayList<String>>(errors));
		return Response.status(status).entity(error).build();
	}	

}
